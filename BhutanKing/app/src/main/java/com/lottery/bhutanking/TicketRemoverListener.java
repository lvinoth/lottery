package com.lottery.bhutanking;

public interface TicketRemoverListener {
    void removeTicket(int position);

}
