package com.lottery.bhutanking;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.lottery.bhutanking.Utils.SharedPreferenceUtility;
import com.lottery.bhutanking.Utils.UpdateBalanceInterface;
import com.lottery.bhutanking.Utils.Utils;

public class DashboardActivity extends AppCompatActivity implements MyListener, UpdateBalanceInterface {

    private TextView walletBalance;
    private TextView userID;
    private LinearLayout addCashLayout;
    private Button homeButton;
    private LinearLayout myHistoryBtn;
    private Button resultsBtn;
    private LinearLayout my_tickets_btn;
    private Button clientEnquiry;
    private ImageView profileBtn;
    private ScrollView parentScroll;
    FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.makeStatusBarTransparent(getWindow(), this);
        setContentView(R.layout.activity_dashboard);
        int bal = SharedPreferenceUtility.getUserBalance(this);
        String id = SharedPreferenceUtility.getUserId(this);
        parentScroll = findViewById(R.id.parentScroll);
        fragmentManager = getSupportFragmentManager();

        clientEnquiry = findViewById(R.id.clientEnquiry);
        clientEnquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFragment(new InquiryFragment(), InquiryFragment.class.getSimpleName());
            }
        });
        my_tickets_btn = findViewById(R.id.my_tickets_btn);
        my_tickets_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFragment(new TodayHistoryFragment(), TodayHistoryFragment.class.getSimpleName());
            }
        });
        resultsBtn = findViewById(R.id.resultsBtn);
        resultsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFragment(new ResultFragment(), ResultFragment.class.getSimpleName());
            }
        });
        myHistoryBtn = findViewById(R.id.myHistoryBtn);
        myHistoryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFragment(new HistoryFragment(), HistoryFragment.class.getSimpleName());
            }
        });
        profileBtn = findViewById(R.id.profileBtn);
        profileBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFragment(new ProfileFragment(), ProfileFragment.class.getSimpleName());
            }
        });
        homeButton = findViewById(R.id.ticketBooking);
        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFragment(new WelcomeFragment(), WelcomeFragment.class.getSimpleName());
            }
        });
        addCashLayout = findViewById(R.id.addCashLayout);
        addCashLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFragment(new AddCashFragment(), AddCashFragment.class.getSimpleName());
            }
        });
        walletBalance = findViewById(R.id.walletBalance);
        userID = findViewById(R.id.userId);
        walletBalance.setText(" ₹ " + bal);
        userID.setText(id);
        String from = getIntent().getStringExtra("source");
        if (from != null && from.equalsIgnoreCase("Register")) {
            loadFragment(new ProfileFragment(), ProfileFragment.class.getSimpleName());
        } else {
            loadFragment(new WelcomeFragment(), WelcomeFragment.class.getSimpleName());
        }
    }

    private void loadFragment(Fragment fragment, String fragmentName) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, fragment, fragmentName);
        fragmentTransaction.commit();
        parentScroll.smoothScrollTo(0, 0);
        parentScroll.scrollTo(0, 0);
    }

    public void updateBalance(int bal) {
        walletBalance.setText(String.valueOf(" ₹ " + bal));
    }


    @Override
    public void replaceFragments(String fragmentName, String type) {
        if (fragmentName.contains("buy")) {
            loadFragment(new BuyTicketBKFragment(type), BuyTicketBKFragment.class.getSimpleName());
        }
        if (fragmentName.contains("weekly")) {
            loadFragment(new WeeklyPriceFragment(), WeeklyPriceFragment.class.getSimpleName());
        }
        if (fragmentName.contains("add_cash")) {
            loadFragment(new AddCashFragment(), AddCashFragment.class.getSimpleName());
        }
        if (fragmentName.contains("home")) {
            loadFragment(new WelcomeFragment(), WelcomeFragment.class.getSimpleName());
        }

    }

    @Override
    public void updateUserBalance(int balance) {
        walletBalance.setText(String.valueOf(" ₹ " + balance));
    }

    private void confirmLogout() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_alert);

        WindowManager.LayoutParams wlp = dialog.getWindow().getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_FULLSCREEN;
        dialog.getWindow().setAttributes(wlp);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
        TextView desc = dialog.findViewById(R.id.alertText);
        TextView cancelBtn = dialog.findViewById(R.id.alertCancel);
        TextView okBtn = dialog.findViewById(R.id.alertOk);

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Invoke Delete API here
                finish();
                dialog.cancel();
            }
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
    }

    @Override
    public void onBackPressed() {
        WelcomeFragment welcomeFragment = (WelcomeFragment) fragmentManager.findFragmentByTag(WelcomeFragment.class.getSimpleName());
        if (welcomeFragment != null && welcomeFragment.isVisible()) {
            // add your code here
            confirmLogout();
        } else {
            loadFragment(new WelcomeFragment(), WelcomeFragment.class.getSimpleName());
        }
    }
}
