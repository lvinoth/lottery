package com.lottery.bhutanking.Model;

import java.util.List;

/**
 * Created by shastamobiledev on 03/04/18.
 */

public class LoginModel {

    private List<Response> response;

    public List<Response> getResponse() {
        return response;
    }

    public void setResponse(List<Response> response) {
        this.response = response;
    }

    public class Response
    {

        private String token;
        private String name;
        private String email;
        private String reg_id;
        private int wallet_amt;
        private String user_status;
        private String status;
        private String message;


        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getReg_id() {
            return reg_id;
        }

        public void setReg_id(String reg_id) {
            this.reg_id = reg_id;
        }

        public int getWallet_amt() {
            return wallet_amt;
        }

        public void setWallet_amt(int wallet_amt) {
            this.wallet_amt = wallet_amt;
        }

        public String getUser_status() {
            return user_status;
        }

        public void setUser_status(String user_status) {
            this.user_status = user_status;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

}
