package com.lottery.bhutanking;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;

import androidx.appcompat.app.AppCompatActivity;
import com.lottery.bhutanking.Utils.SharedPreferenceUtility;
import com.lottery.bhutanking.Utils.Utils;

public class SplashActivity extends AppCompatActivity {
    private Handler handler = new Handler();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.makeStatusBarTransparent(getWindow(), this);
        setContentView(R.layout.activity_splash);
        showMainScreen();
    }
    private void showMainScreen() {

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Launch Main Activity here

                if (TextUtils.isEmpty(SharedPreferenceUtility.getUserId(getApplicationContext()))) {
                    Intent i = new Intent(getBaseContext(), LoginActivity.class);
                    startActivity(i);
                } else {
                    Intent i = new Intent(getBaseContext(), DashboardActivity.class);
                    startActivity(i);
                }

                finish();
            }
        }, 2000);
    }
}
