package com.lottery.bhutanking.Model;

/**
 * Created by TCIG_PC_54 on 8/22/2017.
 */

public class BoardModel {
    boolean isHeader;
    String name;

    public BoardModel(boolean isHeader, String name) {
        this.isHeader = isHeader;
        this.name = name;
    }

    public boolean isHeader() {
        return isHeader;
    }

    public void setHeader(boolean header) {
        isHeader = header;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
