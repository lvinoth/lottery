package com.lottery.bhutanking.Utils;


import com.lottery.bhutanking.Model.HistoryModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

public class DateComparatorPayment implements Comparator<HistoryModel.Transactions>
{
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    @Override
    public int compare(HistoryModel.Transactions lhs, HistoryModel.Transactions rhs)
    {
        try {
            Date arg0Date = dateFormat.parse(lhs.getTransaction_datetime());
            Date arg1Date = dateFormat.parse(rhs.getTransaction_datetime());
            return arg0Date.compareTo(arg1Date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }
}