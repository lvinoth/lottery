package com.lottery.bhutanking.Utils;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.widget.Toast;

import androidx.core.content.FileProvider;

import com.lottery.bhutanking.MainActivity;

import java.io.File;
import java.io.IOException;

public class DownloadFile extends AsyncTask<String, Void, Void> {

    private String filePath;
    private Context mContext;
    public DownloadFile(Context context)
    {
        this.mContext = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Utils.showBusyAnimation((Activity) mContext,"");
    }

    @Override
    protected Void doInBackground(String... strings) {
        String fileUrl = strings[0];   // -> http://maven.apache.org/maven-1.x/maven.pdf
        String fileName = strings[1];  // -> maven.pdf
        String extStorageDirectory = mContext.getExternalFilesDir(Environment.DIRECTORY_DCIM).getAbsolutePath();
        File folder = new File(extStorageDirectory, "Lottery_Result");
        boolean result = folder.mkdirs();

        File pdfFile = new File(folder, fileName);
        filePath = pdfFile.getAbsolutePath();

        try{
            boolean result1 =pdfFile.createNewFile();
        }catch (IOException e){
            e.printStackTrace();
        }
        FileDownloader.downloadFile(fileUrl, pdfFile);
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        Utils.hideBusyAnimation((Activity) mContext);
        File pdfFile = new File(filePath);  // -> filename = maven.pdf
        Uri path = FileProvider.getUriForFile(mContext, mContext.getApplicationContext().getPackageName() + ".provider", pdfFile);
        Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
        pdfIntent.setDataAndType(path, "application/pdf");
        pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        pdfIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        try{
            this.mContext.startActivity(pdfIntent);
        }catch(ActivityNotFoundException e){
            Toast.makeText(this.mContext, "No Application available to view PDF", Toast.LENGTH_SHORT).show();
        }

    }
}
