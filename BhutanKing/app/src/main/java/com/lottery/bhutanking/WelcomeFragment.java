package com.lottery.bhutanking;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.os.CountDownTimer;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.lottery.bhutanking.Model.NextDrawDetailsModel;
import com.lottery.bhutanking.Model.ResultModel;
import com.lottery.bhutanking.Retrofit.ApiClient;
import com.lottery.bhutanking.Retrofit.ApiInterface;
import com.lottery.bhutanking.Utils.SharedPreferenceUtility;
import com.lottery.bhutanking.Utils.UpdateBalanceInterface;
import com.lottery.bhutanking.Utils.Utils;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.widget.Toast.LENGTH_LONG;
import static android.widget.Toast.LENGTH_SHORT;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link WelcomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class WelcomeFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = WelcomeFragment.class.getSimpleName();
    private TextView bkTimerTxt;
    private TextView klTimerTxt;
    private LinearLayout buy_bk_lottery;
    private LinearLayout buy_kl_lottery;
    private Activity baseActivity;
    private Toast mToast;
    private ApiInterface apiService;
    private ImageView showWeeklyBumper;
    private TextView bkReopenString;
    private TextView klReopenString;
    private TextView weeklyReopenString;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private TextView schemeDetails;

    public WelcomeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment WelcomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static WelcomeFragment newInstance(String param1, String param2) {
        WelcomeFragment fragment = new WelcomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//        test();
        View root =  inflater.inflate(R.layout.fragment_welcome, container, false);
        baseActivity = getActivity();
        apiService = ApiClient.getClient().create(ApiInterface.class);
        weeklyReopenString = root.findViewById(R.id.weeklyReopenString);
        bkReopenString = root.findViewById(R.id.bkReopenString);
        klReopenString = root.findViewById(R.id.klReopenString);
        schemeDetails = root.findViewById(R.id.schemeDetails);
        schemeDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MyListener)getActivity()).replaceFragments("weekly","");
            }
        });
        showWeeklyBumper = root.findViewById(R.id.showWeeklyBumper);
        showWeeklyBumper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showToastAtCentre("Coming Soon", LENGTH_SHORT);
            }
        });
        bkTimerTxt = root.findViewById(R.id.bk_timer);
        klTimerTxt = root.findViewById(R.id.kl_timer);
        buy_bk_lottery = root.findViewById(R.id.buy_bk_lottery);
        buy_bk_lottery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MyListener)getActivity()).replaceFragments("buy","bk");
            }
        });
        buy_kl_lottery = root.findViewById(R.id.buy_kl_lottery);
        buy_kl_lottery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MyListener)getActivity()).replaceFragments("buy","kl");
            }
        });
        getNextDrawDetails();
        return root;
    }

    private void getNextDrawDetails()
    {
        Utils.showBusyAnimation(baseActivity,"Authenticating..");
        String token = SharedPreferenceUtility.getUserToken(getActivity());

        Call<NextDrawDetailsModel> call = apiService.getNextDrawDetails(token,"dashboard");

        call.enqueue(new Callback<NextDrawDetailsModel>() {
            @Override
            public void onResponse(Call<NextDrawDetailsModel> call, Response<NextDrawDetailsModel> response) {

                Utils.hideBusyAnimation(baseActivity);
                if(response.body() != null) {
                    if (response.body().getResponse().get(0).getStatus().equalsIgnoreCase("success"))
                    {
                        int bal = response.body().getResponse().get(0).getAvailable_balance();
                        SharedPreferenceUtility.setUserBalance(baseActivity,bal);
                        ((UpdateBalanceInterface)getActivity()).updateUserBalance(bal);
                        String time = response.body().getResponse().get(0).getTime_slots().get(0).getNext_time_slot();
                        if (TextUtils.isEmpty(time.trim()))
                        {
                            buy_bk_lottery.setEnabled(false);
                            bkTimerTxt.setText("Closed");
                            bkReopenString.setVisibility(View.VISIBLE);
                        }
                        else {
                            bkReopenString.setVisibility(View.GONE);
                            buy_bk_lottery.setEnabled(true);
                            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
                            try {
                                String time1 = dateFormat.format(new Date());

                                Date date = dateFormat.parse(time);
                                Date date1 = dateFormat.parse(time1);
                                long difference =  date.getTime() - date1.getTime();

                                long Minutes = difference / (60 * 1000) % 60;
                                long minutesInMillis = TimeUnit.MINUTES.toMillis(Minutes);

                                long secs = difference / 1000;
                                long secsInMillis = TimeUnit.SECONDS.toMillis(secs % 60);

                                // close the timer 10 mins before
                                long HoursToMins = (difference / ( 60 * 60 * 1000) %24) * 60;
                                long millies = (difference - 600000 );

                                if (millies < 0)
                                {
                                    bkIntervalCounter(minutesInMillis + secsInMillis);
                                }
                                else {
                                    BKCountdownTimer(millies);
                                }

                            } catch (ParseException e) {
                            }
                        }

                        String kl_time = response.body().getResponse().get(0).getTime_slots().get(1).getNext_time_slot();
                        if (TextUtils.isEmpty(kl_time.trim()))
                        {
                            buy_kl_lottery.setEnabled(false);
                            klTimerTxt.setText("Closed");
                            klReopenString.setVisibility(View.VISIBLE);
                        }
                        else {
                            klReopenString.setVisibility(View.GONE);
                            buy_kl_lottery.setEnabled(true);
                            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
                            try {
                                String time1 = dateFormat.format(new Date());

                                Date date = dateFormat.parse(kl_time);
                                Date date1 = dateFormat.parse(time1);
                                long difference =  date.getTime() - date1.getTime();

                                long Minutes = difference / (60 * 1000) % 60;
                                long minutesInMillis = TimeUnit.MINUTES.toMillis(Minutes);
                                long secs = difference / 1000;
                                long secsInMillis = TimeUnit.SECONDS.toMillis(secs % 60);

                                // close the timer 10 mins before
                                long HoursToMins = (difference / ( 60 * 60 * 1000) % 24) * 60;
                                long millies = (difference - 600000 );

                                if (millies < 0)
                                {
                                    klIntervalCounter(minutesInMillis + secsInMillis);
                                }
                                else {
                                    KLCountdownTimer(millies);
                                }

                            } catch (ParseException e) {
                            }
                        }

                    }
                    else
                    {
                        String mess = response.body().getResponse().get(0).getMessage();
                        Log.d(TAG, "Response: " + mess);
                        showToastAtCentre("" + mess, LENGTH_LONG);
                    }
                }

            }

            @Override
            public void onFailure(Call<NextDrawDetailsModel> call, Throwable t) {
                Utils.hideBusyAnimation(baseActivity);
                Log.d(TAG, "Response: " + t.getMessage());
                if (t instanceof IOException) {
                    Toast.makeText(baseActivity, "No Internet Connection!!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();
        if (bk_countDownTimer != null)
        {
            bk_countDownTimer.cancel();
            bk_countDownTimer = null;
        }
        if (bkintervalCountDownTimer != null) {
            bkintervalCountDownTimer.cancel();
            bkintervalCountDownTimer = null;
        }
        if (kl_countDownTimer != null)
        {
            kl_countDownTimer.cancel();
            kl_countDownTimer = null;
        }
        if (klIntervalCountDownTimer != null) {
            klIntervalCountDownTimer.cancel();
            klIntervalCountDownTimer = null;
        }
    }

    private CountDownTimer bk_countDownTimer;
    private void BKCountdownTimer(long timer)
    {
        if (bk_countDownTimer != null)
        {
            bk_countDownTimer.cancel();
            bk_countDownTimer = null;
        }
        bk_countDownTimer = new CountDownTimer(timer, 1000) {

            public void onTick(long millisUntilFinished) {
                //here you can have your logic to set text to edittext
                long millis = millisUntilFinished;
                @SuppressLint("DefaultLocale") String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
                        TimeUnit.MILLISECONDS.toMinutes(millis) % TimeUnit.HOURS.toMinutes(1),
                        TimeUnit.MILLISECONDS.toSeconds(millis) % TimeUnit.MINUTES.toSeconds(1));
                bkTimerTxt.setText(hms);
            }

            public void onFinish() {
                buy_bk_lottery.setEnabled(false);
                bkTimerTxt.setText("Waiting");
                bkIntervalCounter(600000);
            }

        }.start();
    }

    private CountDownTimer bkintervalCountDownTimer;
    private void bkIntervalCounter(long time)
    {
        if (bkintervalCountDownTimer != null) {
            bkintervalCountDownTimer.cancel();
            bkintervalCountDownTimer = null;
        }
        bkintervalCountDownTimer = new CountDownTimer(time, 1000) {

            public void onTick(long millisUntilFinished) {
                bkTimerTxt.setText("Waiting");
                buy_bk_lottery.setEnabled(false);
            }

            public void onFinish() {
                getNextDrawDetails();
                bkTimerTxt.setText("");
            }

        }.start();
    }

    private CountDownTimer kl_countDownTimer;
    private void KLCountdownTimer(long timer)
    {
        if (kl_countDownTimer != null)
        {
            kl_countDownTimer.cancel();
            kl_countDownTimer = null;
        }
        kl_countDownTimer = new CountDownTimer(timer, 1000) {

            public void onTick(long millisUntilFinished) {
                //here you can have your logic to set text to edittext
                long millis = millisUntilFinished;
                @SuppressLint("DefaultLocale") String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
                        TimeUnit.MILLISECONDS.toMinutes(millis) % TimeUnit.HOURS.toMinutes(1),
                        TimeUnit.MILLISECONDS.toSeconds(millis) % TimeUnit.MINUTES.toSeconds(1));
                klTimerTxt.setText(hms);
            }

            public void onFinish() {
                buy_kl_lottery.setEnabled(false);
                klTimerTxt.setText("Waiting");
                klIntervalCounter(600000);
            }

        }.start();
    }

    private CountDownTimer klIntervalCountDownTimer;
    private void klIntervalCounter(long time)
    {
        if (klIntervalCountDownTimer != null) {
            klIntervalCountDownTimer.cancel();
            klIntervalCountDownTimer = null;
        }
        klIntervalCountDownTimer = new CountDownTimer(time, 1000) {

            public void onTick(long millisUntilFinished) {
                buy_kl_lottery.setEnabled(false);
                klTimerTxt.setText("Waiting");
            }

            public void onFinish() {
                getNextDrawDetails();
                klTimerTxt.setText("");
            }

        }.start();
    }

    private void showToastAtCentre(String message, int duration) {
        if (mToast != null) {
            mToast.cancel();
            mToast = null;
        }
        mToast = Toast.makeText(getActivity().getApplicationContext(), message, duration);
        mToast.show();
    }

}
