package com.lottery.bhutanking.Retrofit;

import com.lottery.bhutanking.Model.AddCashModel;
import com.lottery.bhutanking.Model.GeneralModel;
import com.lottery.bhutanking.Model.HistoryModel;
import com.lottery.bhutanking.Model.LoginModel;
import com.lottery.bhutanking.Model.NextDrawDetailsModel;
import com.lottery.bhutanking.Model.ProfileModel;
import com.lottery.bhutanking.Model.RegistrationModel;
import com.lottery.bhutanking.Model.ResultModel;
import com.lottery.bhutanking.Model.SubCategoryModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("api/")
    Call<LoginModel> registerUser(@Field("email") String email, @Field("password") String password, @Field("method") String method,
                                         @Field("name") String name,
                                         @Field("mobile") String mobile);
    @FormUrlEncoded
    @POST("api/")
    Call<LoginModel> authenticateLogin(@Field("username") String username, @Field("password") String password, @Field("method") String method);

    @FormUrlEncoded
    @POST("api/")
    Call<ProfileModel> getProfileDetails(@Field("token") String token, @Field("method") String method);

    @FormUrlEncoded
    @POST("api/")
    Call<GeneralModel> updateUserDetails(@Field("token") String token, @Field("method") String method, @Field("new_password") String newPassword);

    @FormUrlEncoded
    @POST("api/")
    Call<GeneralModel> updateBankDetails(@Field("token") String token, @Field("method") String method,
                                         @Field("bank_name") String bankName,
                                         @Field("holder_name") String holderName,
                                         @Field("account_number") String accNum,
                                         @Field("account_type") String accType,
                                         @Field("ifsc_code") String ifscCode,
                                         @Field("upi_code") String upiCode);

    @FormUrlEncoded
    @POST("api/")
    Call<HistoryModel> getHistory(@Field("token") String token, @Field("method") String method);

    @FormUrlEncoded
    @POST("api/")
    Call<ResultModel> getResults(@Field("token") String token, @Field("method") String method);

    @FormUrlEncoded
    @POST("api/")
    Call<NextDrawDetailsModel> getNextDrawDetails(@Field("token") String token, @Field("method") String method);

    @FormUrlEncoded
    @POST("api/")
    Call<GeneralModel> buyTickets(@Field("token") String token, @Field("method") String method, @Field("ticket_details") String ticketDetails);

    @FormUrlEncoded
    @POST("api/")
    Call<AddCashModel> getAddCashLinks(@Field("token") String token, @Field("method") String method);

    @FormUrlEncoded
    @POST("api/")
    Call<GeneralModel> getForgotPassword(@Field("method") String method, @Field("email") String email);

    @FormUrlEncoded
    @POST("api/")
    Call<GeneralModel> submitEnquiry(@Field("token") String token, @Field("method") String method,
                                         @Field("reg_id") String reg_id,
                                         @Field("name") String name,
                                         @Field("user_type") String user_type,
                                         @Field("mobile") String mobile,
                                         @Field("email") String email,
                                         @Field("comments") String comments);
}
