package com.lottery.bhutanking.Utils;


import com.lottery.bhutanking.Model.HistoryModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;
import java.util.function.ToIntFunction;
import java.util.function.ToLongFunction;

public class DateComparator implements Comparator<HistoryModel.Ticket_Purchase_History>
{
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    @Override
    public int compare(HistoryModel.Ticket_Purchase_History lhs, HistoryModel.Ticket_Purchase_History rhs)
    {
        try {
            Date arg0Date = dateFormat.parse(lhs.getPurchase_date());
            Date arg1Date = dateFormat.parse(rhs.getPurchase_date());
            return arg0Date.compareTo(arg1Date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }
}