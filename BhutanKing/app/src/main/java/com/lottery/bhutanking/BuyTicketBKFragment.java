package com.lottery.bhutanking;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.CountDownTimer;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.lottery.bhutanking.Adapter.BoardAdapter;
import com.lottery.bhutanking.Adapter.BuyTicketAdapter;
import com.lottery.bhutanking.Model.BoardModel;
import com.lottery.bhutanking.Model.GeneralModel;
import com.lottery.bhutanking.Model.NextDrawDetailsModel;
import com.lottery.bhutanking.Model.TicketModel;
import com.lottery.bhutanking.Retrofit.ApiClient;
import com.lottery.bhutanking.Retrofit.ApiInterface;
import com.lottery.bhutanking.Utils.SharedPreferenceUtility;
import com.lottery.bhutanking.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.widget.Toast.LENGTH_LONG;
import static android.widget.Toast.LENGTH_SHORT;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link BuyTicketBKFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BuyTicketBKFragment extends Fragment implements TicketRemoverListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = BuyTicketBKFragment.class.getSimpleName();
    private Spinner selectBoardSpinner;
    private Button updateTicketDetails;
    private Button buyTicketBtn;
    private EditText ticketNo;
    private EditText ticketQuantity;
    private String selectedBoard;
    List<BoardModel> listItems;
    private Activity baseActivity;
    private Toast mToast;
    private ApiInterface apiService;
    private List<TicketModel> newTicketList = new ArrayList<>();
    private BuyTicketAdapter buyTicketAdapter;
    private RecyclerView ticketsRecyclerView;
    private TextView totalPrize;
    private TextView ticketCost;
    private TextView lotteryPrize;
    private TextView lottery_timer;
    private TextView ticketPrize;

    private TextView bkReopenString;

    private String lotteryType;
    private ImageView bkLogoTitle;
    private RelativeLayout klLogoTitle;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public BuyTicketBKFragment(String type) {
        // Required empty public constructor
        lotteryType = type;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BuyTicketBKFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BuyTicketBKFragment newInstance(String param1, String param2) {
        BuyTicketBKFragment fragment = new BuyTicketBKFragment("");
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_buy_ticket_bk1, container, false);
        baseActivity = getActivity();
        apiService = ApiClient.getClient().create(ApiInterface.class);
        newTicketList.clear();
        bkReopenString = root.findViewById(R.id.bkReopenString);
        ticketsRecyclerView = root.findViewById(R.id.addTicketsRecyclerView);
        selectBoardSpinner = root.findViewById(R.id.select_board);
        listItems = lotteryType.equalsIgnoreCase("bk") ? getBoardListForBK(true) : getBoardListForBK(false);

        BoardAdapter boardAdapter = new BoardAdapter(getActivity(), R.layout.spinner_item, listItems);
        totalPrize = root.findViewById(R.id.totalPrize);
        lotteryPrize = root.findViewById(R.id.prize);
        ticketPrize = root.findViewById(R.id.ticketPrize);
        lottery_timer = root.findViewById(R.id.lottery_timer);
        bkLogoTitle = root.findViewById(R.id.bkLogoTitle);
        klLogoTitle = root.findViewById(R.id.klLogoTitle);
        if (lotteryType.equalsIgnoreCase("bk"))
        {
            bkLogoTitle.setVisibility(View.VISIBLE);
            klLogoTitle.setVisibility(View.GONE);
        }
        else {
            bkLogoTitle.setVisibility(View.GONE);
            klLogoTitle.setVisibility(View.VISIBLE);
        }
        bkLogoTitle.setVisibility(lotteryType.equalsIgnoreCase("bk") ? View.VISIBLE : View.GONE);
        klLogoTitle.setVisibility(!lotteryType.equalsIgnoreCase("bk") ? View.VISIBLE : View.GONE);
        ticketCost = root.findViewById(R.id.ticketCost);
        selectBoardSpinner.setAdapter(boardAdapter);
        selectBoardSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                selectedBoard = listItems.get(position).getName();
                ticketNo.setText("");
                ticketQuantity.setText("");
                ticketPrize.setText("Rs 0");
                int MAX_NUM = 0;
                if (selectedBoard.equalsIgnoreCase("Select Board"))
                {
                    MAX_NUM  = 0;
                    ticketNo.setHint("0-9");
                    ticketCost.setText("-");
                    lotteryPrize.setText("-");
                    ticketNo.setFilters(new InputFilter[] { new InputFilter.LengthFilter(0) });
                    ticketQuantity.setFilters(new InputFilter[] { new InputFilter.LengthFilter(0) });
                    return;
                }

                if (selectedBoard.equalsIgnoreCase("4 Digit"))
                {
                    selectedBoard = "XABC";
                }

                if (selectedBoard.length() == 1) {
                    MAX_NUM  = 1;
                    ticketNo.setHint("0-9");
                    ticketCost.setText("12");
                    lotteryPrize.setText("100");
                }
                if (selectedBoard.length() == 2) {
                    MAX_NUM  = 2;
                    ticketNo.setHint("00-99");
                    ticketCost.setText("15");
                    lotteryPrize.setText("1000");
                }
                if (selectedBoard.length() == 3) {
                    MAX_NUM  = 3;
                    ticketNo.setHint("000-999");
                    ticketCost.setText("60");
                    lotteryPrize.setText("27000");
                }
                if (selectedBoard.length() == 4) {
                    MAX_NUM  = 4;
                    ticketNo.setHint("0000-9999");
                    ticketCost.setText("100");
                    lotteryPrize.setText("100000");
                }
                ticketQuantity.setFilters(new InputFilter[] { new InputFilter.LengthFilter(4) });
                ticketNo.setFilters(new InputFilter[] { new InputFilter.LengthFilter(MAX_NUM) });
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
        buyTicketBtn = root.findViewById(R.id.buyTicketBtn);
        buyTicketBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (newTicketList.size() == 0) {
                    showToastAtCentre("Please select the tickets to buy", LENGTH_SHORT);
                    return;
                }
                int total = 0;
                for (int i = 0; i < newTicketList.size(); i++) {
                    total += Integer.parseInt(newTicketList.get(i).getTicket_price());
                }
                int balance = SharedPreferenceUtility.getUserBalance(baseActivity);
                if (total > balance) {
                    //showToastAtCentre("Insufficient balance in Wallet. Please add cash to buy tickets", LENGTH_SHORT);
                    promptAddCash();
                    return;
                }

                buyTickets();
            }
        });
        buyTicketAdapter = new BuyTicketAdapter(getActivity(), newTicketList, this);
        ticketNo = root.findViewById(R.id.ticketNo);
        ticketQuantity = root.findViewById(R.id.ticketQuantity);
        ticketQuantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (count != 0 && !ticketCost.getText().toString().trim().equalsIgnoreCase("-")) {
                    int prize = Integer.parseInt(ticketCost.getText().toString().trim()) * Integer.parseInt(s.toString());
                    ticketPrize.setText("Rs " + String.valueOf(prize));
                }
                else{
                    ticketPrize.setText("Rs 0");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        ticketQuantity.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    updateTicketDetails.performClick();
                    return true;
                }
                return false;
            }
        });
        updateTicketDetails = root.findViewById(R.id.updateTicketDetails);
        updateTicketDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedBoard.equalsIgnoreCase("Select Board"))
                {
                    showToastAtCentre("Please select any board", LENGTH_SHORT);
                    return;
                }
                if (TextUtils.isEmpty(ticketNo.getText().toString().trim())) {
                    showToastAtCentre("Please enter the ticket number", LENGTH_SHORT);
                    return;
                }
                if (TextUtils.isEmpty(ticketQuantity.getText().toString().trim())) {
                    showToastAtCentre("Please enter the ticket quantity", LENGTH_SHORT);
                    return;
                }
                if (selectedBoard.equalsIgnoreCase("4 Digit"))
                {
                    selectedBoard = "XABC";
                }
                int value = Integer.parseInt(ticketNo.getText().toString().trim());
                int price = 12;
                if (selectedBoard.length() == 1) {
                    price = 12;
                    ticketCost.setText("12");
                    lotteryPrize.setText("100");
                    if (value > 9) {
                        showToastAtCentre("Please Enter a number between 0 - 9", LENGTH_SHORT);
                        return;
                    }
                }
                if (selectedBoard.length() == 2) {
                    price = 15;
                    ticketCost.setText("15");
                    lotteryPrize.setText("1000");
                    if (value < 9 || value > 99) {
                        showToastAtCentre("Please Enter a number between 00 - 99", LENGTH_SHORT);
                        return;
                    }
                }
                if (selectedBoard.length() == 3) {
                    price = 60;
                    ticketCost.setText("60");
                    lotteryPrize.setText("27000");
                    if (value < 99 || value > 999) {
                        showToastAtCentre("Please Enter a number between 000 - 999", LENGTH_SHORT);
                        return;
                    }
                }
                if (selectedBoard.length() == 4) {
                    price = 100;
                    ticketCost.setText("100");
                    lotteryPrize.setText("100000");
                    if (value < 999 || value > 9999) {
                        showToastAtCentre("Please Enter a number between 0000 - 9999", LENGTH_SHORT);
                        return;
                    }
                }
                if (Integer.parseInt(ticketQuantity.getText().toString().trim()) > 1000) {
                    showToastAtCentre("A Maximum of 1000 tickets can be purchased. Please Enter a value within 1000", LENGTH_SHORT);
                    return;
                }
                TicketModel ticket = new TicketModel();
                ticket.setTicket_count(ticketQuantity.getText().toString().trim());
                ticket.setTicket_no(value + "");
                ticket.setTicket_no_position(selectedBoard.trim());
                int tot = Integer.parseInt(ticketQuantity.getText().toString().trim()) * price;
                ticket.setTicket_price(String.valueOf(tot));
                newTicketList.add(ticket);
                addTicketDetails();
                ticketQuantity.setText("");
                ticketNo.setText("");
            }
        });
        getNextDrawDetails();
        return root;
    }

    private List<BoardModel> getBoardListForBK(boolean isBK)
    {
        List<BoardModel> wordList = new ArrayList<>();

        wordList.add(new BoardModel(false, "Select Board"));
        wordList.add(new BoardModel(true, "1 Digit"));
        wordList.add(new BoardModel(false, "A"));
        wordList.add(new BoardModel(false, "B"));
        wordList.add(new BoardModel(false, "C"));
        wordList.add(new BoardModel(true, "2 Digit"));
        wordList.add(new BoardModel(false, "AB"));
        wordList.add(new BoardModel(false, "AC"));
        wordList.add(new BoardModel(false, "BC"));
        wordList.add(new BoardModel(true, "3 Digit"));
        wordList.add(new BoardModel(false, "ABC"));
        if (!isBK)
        {
            wordList.add(new BoardModel(false, "4 Digit"));
//            wordList.add(new BoardModel(false, "XABC"));
        }
        return wordList;
    }

    private void buyTickets() {
        Utils.showBusyAnimation(baseActivity, "Authenticating..");
        String token = SharedPreferenceUtility.getUserToken(getActivity());

        String methodType = "ticket_purchase_bk";
        String header = "bs_tickets";
        if (lotteryType.equalsIgnoreCase("kl"))
        {
            methodType = "ticket_purchase_kl";
            header = "kl_tickets";
        }

        String ticketDetails = createTicketJson(header).toString();

        Call<GeneralModel> call = apiService.buyTickets(token, methodType, ticketDetails);

        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {


                if (response.body() != null) {
                    if (response.body().getResponse().get(0).getStatus().equalsIgnoreCase("success")) {
                        showToastAtCentre("Tickets booked successfully!!", LENGTH_LONG);
                        newTicketList.clear();
                        totalPrize.setText("-");
                        buyTicketAdapter.notifyDataSetChanged();
                        getWalletBalance();
                    } else // Handle failure cases here
                    {
                        Utils.hideBusyAnimation(baseActivity);
                        String mess = response.body().getResponse().get(0).getMessage();
                        Log.d(TAG, "Response: " + mess);
                        showToastAtCentre("" + mess, LENGTH_LONG);
                    }
                }

            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                Utils.hideBusyAnimation(baseActivity);
                Log.d(TAG, "Response: " + t.getMessage());
                if (t instanceof IOException) {
                    Toast.makeText(baseActivity, "No Internet Connection!!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void addTicketDetails() {
        buyTicketAdapter.setDataSet(newTicketList);
        buyTicketAdapter.notifyDataSetChanged();
        ticketsRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        ticketsRecyclerView.setAdapter(buyTicketAdapter);
        int total = 0;
        for (int i = 0; i < newTicketList.size(); i++) {
            total += Integer.parseInt(newTicketList.get(i).getTicket_price());
        }
        totalPrize.setText(String.valueOf(total));
    }

    private void promptAddCash() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_alert);

        WindowManager.LayoutParams wlp = dialog.getWindow().getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_FULLSCREEN;
        dialog.getWindow().setAttributes(wlp);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
        TextView desc = dialog.findViewById(R.id.alertText);
        TextView cancelBtn = dialog.findViewById(R.id.alertCancel);
        TextView okBtn = dialog.findViewById(R.id.alertOk);

        desc.setText("Insufficient balance in Wallet. Please add cash to buy tickets");
        okBtn.setText("Add Cash");
        cancelBtn.setText("Cancel");

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Invoke Delete API here
                ((MyListener)getActivity()).replaceFragments("add_cash","");
                dialog.cancel();
            }
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
    }

    private JSONObject createTicketJson(String header) {
        JSONObject finalObject = new JSONObject();
        try {
            JSONArray resultSet = new JSONArray();
            JSONObject rowObject;
            for (int i = 0; i < this.newTicketList.size(); i++) {
                rowObject = new JSONObject();
                rowObject.put("ticket_no", Integer.parseInt(newTicketList.get(i).getTicket_no()));
                rowObject.put("ticket_no_position", newTicketList.get(i).getTicket_no_position());
                rowObject.put("ticket_count", Integer.parseInt(newTicketList.get(i).getTicket_count()));
                rowObject.put("ticket_price", Integer.parseInt(newTicketList.get(i).getTicket_price()));
                resultSet.put(rowObject);
            }

            finalObject.put(header, resultSet);
        } catch (Exception ex) {

        }
        return finalObject;
    }

    private void getNextDrawDetails() {
        Utils.showBusyAnimation(baseActivity, "Authenticating..");
        String token = SharedPreferenceUtility.getUserToken(getActivity());

        Call<NextDrawDetailsModel> call = apiService.getNextDrawDetails(token, "dashboard");

        call.enqueue(new Callback<NextDrawDetailsModel>() {
            @Override
            public void onResponse(Call<NextDrawDetailsModel> call, Response<NextDrawDetailsModel> response) {

                Utils.hideBusyAnimation(baseActivity);
                if (response.body() != null) {
                    if (response.body().getResponse().get(0).getStatus().equalsIgnoreCase("success")) {
                        String time = response.body().getResponse().get(0).getTime_slots().get(0).getNext_time_slot();
                        if (!lotteryType.equalsIgnoreCase("bk")) {
                            time = response.body().getResponse().get(0).getTime_slots().get(1).getNext_time_slot();
                        }
                        if (TextUtils.isEmpty(time))
                        {
                            lottery_timer.setText("Closed");
                            buyTicketBtn.setEnabled(false);
                            bkReopenString.setVisibility(View.VISIBLE);
                            updateTicketDetails.setEnabled(false);
                            return;
                        }
                        bkReopenString.setVisibility(View.GONE);
                        buyTicketBtn.setEnabled(true);
                        updateTicketDetails.setEnabled(true);
                        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
                        try {
                            String time1 = dateFormat.format(new Date());

                            Date date = dateFormat.parse(time);
                            Date date1 = dateFormat.parse(time1);
                            long difference = date.getTime() - date1.getTime();

                            long Minutes = difference / (60 * 1000) % 60;
                            long minutesInMillis = TimeUnit.MINUTES.toMillis(Minutes);

                            long secs = difference / 1000;
                            long secsInMillis = TimeUnit.SECONDS.toMillis(secs % 60);

                            // close the timer 10 mins before
                            long HoursToMins = (difference / (60 * 60 * 1000) % 24) * 60;
                            long millies = (difference - 600000);
//                            Log.e("Time 1:-- ", Minutes+"");
//                            Log.e("Time milli:-- ", millies+"");
                            if (millies < 0)
                            {
                                intervalCounter(minutesInMillis + secsInMillis);
                            }
                            else {
                                BKCountdownTimer(millies);
                            }

                        } catch (ParseException e) {
                        }

                    } else {
                        String mess = response.body().getResponse().get(0).getMessage();
                        Log.d(TAG, "Response: " + mess);
                        showToastAtCentre("" + mess, LENGTH_LONG);
                    }
                }

            }

            @Override
            public void onFailure(Call<NextDrawDetailsModel> call, Throwable t) {
                Utils.hideBusyAnimation(baseActivity);
                Log.d(TAG, "Response: " + t.getMessage());
                if (t instanceof IOException) {
                    Toast.makeText(baseActivity, "No Internet Connection!!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void getWalletBalance() {
        //Utils.showBusyAnimation(baseActivity, "Authenticating..");
        String token = SharedPreferenceUtility.getUserToken(getActivity());

        Call<NextDrawDetailsModel> call = apiService.getNextDrawDetails(token, "dashboard");

        call.enqueue(new Callback<NextDrawDetailsModel>() {
            @Override
            public void onResponse(Call<NextDrawDetailsModel> call, Response<NextDrawDetailsModel> response) {

                Utils.hideBusyAnimation(baseActivity);
                if (response.body() != null) {
                    if (response.body().getResponse().get(0).getStatus().equalsIgnoreCase("success")) {
                        int bal = response.body().getResponse().get(0).getAvailable_balance();
                        SharedPreferenceUtility.setUserBalance(baseActivity,bal);
                        ((DashboardActivity)getActivity()).updateBalance(bal);

                    } else {
                        String mess = response.body().getResponse().get(0).getMessage();
                        Log.d(TAG, "Response: " + mess);
                        showToastAtCentre("" + mess, LENGTH_LONG);
                    }
                }

            }

            @Override
            public void onFailure(Call<NextDrawDetailsModel> call, Throwable t) {
                Utils.hideBusyAnimation(baseActivity);
                Log.d(TAG, "Response: " + t.getMessage());
                if (t instanceof IOException) {
                    Toast.makeText(baseActivity, "No Internet Connection!!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private CountDownTimer bk_countDownTimer;
    private void BKCountdownTimer(long timer) {
        if (bk_countDownTimer != null) {
            bk_countDownTimer.cancel();
            bk_countDownTimer = null;
        }
        bk_countDownTimer = new CountDownTimer(timer, 1000) {

            public void onTick(long millisUntilFinished) {
                //here you can have your logic to set text to edittext
                long millis = millisUntilFinished;
                @SuppressLint("DefaultLocale") String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
                        TimeUnit.MILLISECONDS.toMinutes(millis) % TimeUnit.HOURS.toMinutes(1),
                        TimeUnit.MILLISECONDS.toSeconds(millis) % TimeUnit.MINUTES.toSeconds(1));
                lottery_timer.setText(hms);
            }

            public void onFinish() {
                buyTicketBtn.setEnabled(false);
                lottery_timer.setText("Waiting");
                intervalCounter(600000);
            }

        }.start();
    }

    private CountDownTimer intervalCountDownTimer;
    private void intervalCounter(long time)
    {
        if (intervalCountDownTimer != null) {
            intervalCountDownTimer.cancel();
            intervalCountDownTimer = null;
        }
        intervalCountDownTimer = new CountDownTimer(time, 1000) {

            public void onTick(long millisUntilFinished) {
                lottery_timer.setText("Waiting");
            }

            public void onFinish() {
                getNextDrawDetails();
                lottery_timer.setText("");
            }

        }.start();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (bk_countDownTimer != null)
        {
            bk_countDownTimer.cancel();
            bk_countDownTimer = null;
        }
        if (intervalCountDownTimer != null) {
            intervalCountDownTimer.cancel();
            intervalCountDownTimer = null;
        }
    }

    private void showToastAtCentre(String message, int duration) {
        if (mToast != null) {
            mToast.cancel();
            mToast = null;
        }
        mToast = Toast.makeText(getActivity().getApplicationContext(), message, duration);
        mToast.show();
    }

    @Override
    public void removeTicket(int position) {
        newTicketList.remove(position);
        addTicketDetails();
    }
}
