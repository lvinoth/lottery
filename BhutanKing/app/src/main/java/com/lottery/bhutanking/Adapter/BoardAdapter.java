package com.lottery.bhutanking.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.lottery.bhutanking.Model.BoardModel;
import com.lottery.bhutanking.R;

import java.nio.DoubleBuffer;
import java.util.List;

public class BoardAdapter extends ArrayAdapter<BoardModel> {


    private List<BoardModel> boardModels;
    private Context mContext;

    public BoardAdapter(@NonNull Context context, int resource, @NonNull List<BoardModel> boardModels) {
        super(context, resource, boardModels);
        this.boardModels = boardModels;
        this.mContext = context;
    }

    @Override
    public boolean isEnabled(int position) {
        return !boardModels.get(position).isHeader();
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            Context mContext = this.getContext();
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.spinner_item, null);
        }

        TextView tvName = v.findViewById(R.id.board);
        BoardModel model = boardModels.get(position);
        tvName.setText(model.getName());
        tvName.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
        return v;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            Context mContext = this.getContext();
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.spinner_item, null);
        }

        TextView tvName = v.findViewById(R.id.board);
        BoardModel model = boardModels.get(position);

        if (model.isHeader()){
            tvName.setTextColor(Color.parseColor("#ffffff"));
            tvName.setBackgroundColor(Color.parseColor("#942400"));
            tvName.setPadding(0,10,0,10);
            Typeface type = Typeface.createFromAsset(mContext.getAssets(),"font/tahoma_bold.ttf");
            tvName.setTypeface(type);
//            tvName.setTextSize(16f);
        }
        else {
            tvName.setPadding(0,10,0,10);
        }
        tvName.setText(model.getName());
        return v;
    }
}
