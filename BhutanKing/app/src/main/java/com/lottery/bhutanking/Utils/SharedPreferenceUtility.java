package com.lottery.bhutanking.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.lottery.bhutanking.R;


public class SharedPreferenceUtility {

    private static SharedPreferences sharedPreferences;


    public static SharedPreferences getSharedPreferenceInstance(Context context) {
        if (sharedPreferences == null)
            sharedPreferences = context.getSharedPreferences(
                    context.getString(R.string.USER_PREFERENCES),
                    Context.MODE_PRIVATE);

        return sharedPreferences;
    }

    //* SAVING USER ID & TOKEN *//

    public static void setUserBalance(Context context, int userBalance) {
        SharedPreferences.Editor sharedPreferencesEditor = getSharedPreferenceInstance(context).edit();
        sharedPreferencesEditor.putInt("usr_balance", userBalance);
        sharedPreferencesEditor.commit();
    }

    public static void saveUserToken(Context context, String mailOrPhone, String token, String userId, int userBalance) {
        SharedPreferences.Editor sharedPreferencesEditor = getSharedPreferenceInstance(context).edit();

        sharedPreferencesEditor.putString("usr_mobile", mailOrPhone);
        sharedPreferencesEditor.putString("usr_token", token);
        sharedPreferencesEditor.putString("usr_id", userId);
        sharedPreferencesEditor.putInt("usr_balance", userBalance);
        sharedPreferencesEditor.commit();
    }

    public static void clearUserDetails(Context context) {
        SharedPreferences.Editor sharedPreferencesEditor = getSharedPreferenceInstance(context).edit();

        sharedPreferencesEditor.putString("usr_mobile", "");
        sharedPreferencesEditor.putString("usr_token", "");
        sharedPreferencesEditor.putString("usr_id", "");
        sharedPreferencesEditor.putInt("usr_balance", 0);
        sharedPreferencesEditor.commit();
    }

    public static int getUserBalance(Context context) {
        return getSharedPreferenceInstance(context).getInt("usr_balance", 0);
    }

    public static String getUserId(Context context) {
        return getSharedPreferenceInstance(context).getString("usr_id", "");
    }

    public static String getUserNameOrMobile(Context context) {
        return getSharedPreferenceInstance(context).getString("usr_mobile", "");
    }

    public static String getUserToken(Context context) {
        return getSharedPreferenceInstance(context).getString("usr_token", "");
    }

}
