package com.lottery.bhutanking;

import android.app.Activity;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.lottery.bhutanking.Adapter.TodayPurchaseHistoryAdapter;
import com.lottery.bhutanking.Model.HistoryModel;
import com.lottery.bhutanking.Retrofit.ApiClient;
import com.lottery.bhutanking.Retrofit.ApiInterface;
import com.lottery.bhutanking.Utils.SharedPreferenceUtility;
import com.lottery.bhutanking.Utils.Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.widget.Toast.LENGTH_LONG;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TodayHistoryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TodayHistoryFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private static final String TAG = TodayHistoryFragment.class.getSimpleName();
    private Activity baseActivity;
    private Toast mToast;
    private ApiInterface apiService;
    private RecyclerView ticketHistoryRecyclerView;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public TodayHistoryFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TodayHistoryFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TodayHistoryFragment newInstance(String param1, String param2) {
        TodayHistoryFragment fragment = new TodayHistoryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        baseActivity = getActivity();
        apiService = ApiClient.getClient().create(ApiInterface .class);
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_today_history, container, false);
        ticketHistoryRecyclerView = root.findViewById(R.id.ticketRecyclerView);
        getTicketHistory();
        return root;
    }

    List<HistoryModel.Ticket_Purchase_History> historyTicketPurchaseList = new ArrayList<>();
    TodayPurchaseHistoryAdapter todayPurchaseHistoryAdapter;

    private void getTicketHistory()
    {
        Utils.showBusyAnimation(baseActivity,"Authenticating..");
        String token = SharedPreferenceUtility.getUserToken(getActivity());

        Call<HistoryModel> call = apiService.getHistory(token,"trans_ticket_purchase_history");

        call.enqueue(new Callback<HistoryModel>() {
            @Override
            public void onResponse(Call<HistoryModel> call, Response<HistoryModel> response) {

                Utils.hideBusyAnimation(baseActivity);
                if(response.body() != null) {
                    if (response.body().getResponse().get(0).getStatus().equalsIgnoreCase("success"))
                    {
                        int transactionsCount = response.body().getResponse().get(0).getTransactions().size();

                        int ticketHistoryCount = response.body().getResponse().get(0).getTicket_purchase_history().size();
                        HistoryModel.Ticket_Purchase_History ticketPurchaseHistory;
                        String date = DateFormat.format("dd-MM-yyyy", new Date()).toString();
                        for (int i = 0; i < ticketHistoryCount; i++){
                            ticketPurchaseHistory = (HistoryModel.Ticket_Purchase_History)response.body().getResponse().get(0).getTicket_purchase_history().get(i).get(0);
                            if (ticketPurchaseHistory.getPurchase_date().contains(date)) {
                                historyTicketPurchaseList.add(ticketPurchaseHistory);
                            }
                        }
                        todayPurchaseHistoryAdapter = new TodayPurchaseHistoryAdapter(getActivity(),historyTicketPurchaseList);
                        ticketHistoryRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
                        ticketHistoryRecyclerView.setAdapter(todayPurchaseHistoryAdapter);

                    }
                    else // Handle failure cases here
                    {
                        String mess = response.body().getResponse().get(0).getMessage();
                        Log.d(TAG, "Response: " + mess);
                        showToastAtCentre("" + mess, LENGTH_LONG);
                    }
                }

            }

            @Override
            public void onFailure(Call<HistoryModel> call, Throwable t) {
                Utils.hideBusyAnimation(baseActivity);
                Log.d(TAG, "Response: " + t.getMessage());
                if (t instanceof IOException) {
                    Toast.makeText(baseActivity, "No Internet Connection!!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void showToastAtCentre(String message, int duration) {
        if (mToast != null) {
            mToast.cancel();
            mToast = null;
        }
        mToast = Toast.makeText(getActivity().getApplicationContext(), message, duration);
        mToast.show();
    }
}
