package com.result.bhutanking.Model;

import java.util.List;

public class AddCashModel {

    private List<Response> response;

    public List<Response> getResponse() {
        return response;
    }

    public void setResponse(List<Response> response) {
        this.response = response;
    }


    public class Response
    {
        private String status;
        private String message;
        private List<List<Add_Cash>> add_cash;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<List<Add_Cash>> getAdd_cash() {
            return add_cash;
        }

        public void setAdd_cash(List<List<Add_Cash>> add_cash) {
            this.add_cash = add_cash;
        }
    }

    public class Add_Cash
    {
        private String type;
        private String amount;
        private String link;


        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }
    }
}
