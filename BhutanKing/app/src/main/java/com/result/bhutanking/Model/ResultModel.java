package com.result.bhutanking.Model;

import java.util.HashMap;
import java.util.List;

public class ResultModel {

    private List<Response> response;

    public List<Response> getResponse() {
        return response;
    }

    public void setResponse(List<Response> response) {
        this.response = response;
    }


    public class Response
    {
        private String status;
        private String message;
        private String result_chart;
        private String last_draw_starting_number;
        private HashMap<String, List<List<Result>>> butan_king_result;
        private HashMap<String, List<List<Result>>> kerala_result;
//        private List<List<Transactions>> transactions;
//        private List<List<Ticket_Purchase_History>> ticket_purchase_history;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public HashMap<String, List<List<Result>>> getButan_king_result() {
            return butan_king_result;
        }

        public void setButan_king_result(HashMap<String, List<List<Result>>> butan_king_result) {
            this.butan_king_result = butan_king_result;
        }

        public HashMap<String, List<List<Result>>> getKerala_result() {
            return kerala_result;
        }

        public void setKerala_result(HashMap<String, List<List<Result>>> kerala_result) {
            this.kerala_result = kerala_result;
        }

        public String getResult_chart() {
            return result_chart;
        }

        public void setResult_chart(String result_chart) {
            this.result_chart = result_chart;
        }

        public String getLast_draw_starting_number() {
            return last_draw_starting_number;
        }

        public void setLast_draw_starting_number(String last_draw_starting_number) {
            this.last_draw_starting_number = last_draw_starting_number;
        }
    }

    public class Result
    {
        private String time_slot;
        private String winning_no;

        public String getTime_slot() {
            return time_slot;
        }

        public void setTime_slot(String time_slot) {
            this.time_slot = time_slot;
        }

        public String getWinning_no() {
            return winning_no;
        }

        public void setWinning_no(String winning_no) {
            this.winning_no = winning_no;
        }
    }

}
