package com.result.bhutanking.Model;

import java.util.List;

public class TicketDetailsModel {

    private List<Response> response;

    public List<Response> getResponse() {
        return response;
    }

    public void setResponse(List<Response> response) {
        this.response = response;
    }


    public class Response
    {
        private String status;
        private String message;
        private List<PriceCat> ticket_price;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<PriceCat> getTicket_price() {
            return ticket_price;
        }

        public void setTicket_price(List<PriceCat> ticket_price) {
            this.ticket_price = ticket_price;
        }
    }

    public class PriceCat
    {
        private List<List<Price>> BK;
        private List<List<Price>> KL;
        private List<List<Price>> WEEKLY;

        public List<List<Price>> getBK() {
            return BK;
        }

        public void setBK(List<List<Price>> BK) {
            this.BK = BK;
        }

        public List<List<Price>> getKL() {
            return KL;
        }

        public void setKL(List<List<Price>> KL) {
            this.KL = KL;
        }

        public List<List<Price>> getWEEKLY() {
            return WEEKLY;
        }

        public void setWEEKLY(List<List<Price>> WEEKLY) {
            this.WEEKLY = WEEKLY;
        }
    }


    public class Price
    {
        private int number_of_digits;
        private String ticket_price;
        private String prize;

        public int getNumber_of_digits() {
            return number_of_digits;
        }

        public void setNumber_of_digits(int number_of_digits) {
            this.number_of_digits = number_of_digits;
        }

        public String getTicket_price() {
            return ticket_price;
        }

        public void setTicket_price(String ticket_price) {
            this.ticket_price = ticket_price;
        }

        public String getPrize() {
            return prize;
        }

        public void setPrize(String prize) {
            this.prize = prize;
        }
    }
}
