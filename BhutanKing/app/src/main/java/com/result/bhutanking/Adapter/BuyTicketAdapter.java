package com.result.bhutanking.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.result.bhutanking.DashboardActivity;
import com.result.bhutanking.Model.TicketModel;
import com.result.bhutanking.R;
import com.result.bhutanking.TicketRemoverListener;

import java.util.List;


public class BuyTicketAdapter extends RecyclerView.Adapter<BuyTicketAdapter.ViewHolder>  {

    private List<TicketModel> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context mContext;
    private Activity baseActivity;
    private TicketRemoverListener mListener;

    public void setDataSet(List<TicketModel> data)
    {
        this.mData = data;
    }


    // data is passed into the constructor
    public BuyTicketAdapter(Context context, List<TicketModel> data, TicketRemoverListener listener) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.mContext = context;
        this.baseActivity = (DashboardActivity)context;
        this.mListener = listener;
    }

    // inflates the row layout from xml when needed
    @NonNull
    @Override
    public BuyTicketAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.ticket_booking_list, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final TicketModel transactions = mData.get(position);
        holder.sno.setText(String.valueOf(position + 1));
        holder.ticketQty.setText(transactions.getTicket_count());
        int ticketPositionCount = transactions.getTicket_no_position().length();
        String ticket_no = "";
        /*for (int i = 0; i < ticketPositionCount; i++)
        {
            if (i != 0){
                ticket_no.append("-");
            }
            ticket_no.append(transactions.getTicket_no_position().charAt(i)).append(transactions.getTicket_no().charAt(i));
        }*/
        ticket_no = transactions.getTicket_no_position()+"-"+transactions.getTicket_no();
        if (ticketPositionCount >= 3)
        {
            ticket_no = transactions.getTicket_no();
        }
        holder.ticketNo.setText(ticket_no);

        holder.ticketNo.setText(ticket_no.toString());
        holder.amount.setText("Rs " + transactions.getTicket_price());

        holder.removeTicket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.removeTicket(position);
            }
        });

    }


    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView sno;
        TextView ticketNo;
        TextView ticketQty;
        TextView amount;
        ImageView removeTicket;

        ViewHolder(View itemView) {
            super(itemView);
            sno = itemView.findViewById(R.id.ticket_id);
            ticketQty = itemView.findViewById(R.id.ticket_qty);
            ticketNo = itemView.findViewById(R.id.ticket_no);
            amount = itemView.findViewById(R.id.ticket_amt);
            removeTicket = itemView.findViewById(R.id.ticket_remove);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }


    // convenience method for getting data at click position
    TicketModel getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
