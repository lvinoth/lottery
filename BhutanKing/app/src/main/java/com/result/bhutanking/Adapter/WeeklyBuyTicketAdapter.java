package com.result.bhutanking.Adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.result.bhutanking.DashboardActivity;
import com.result.bhutanking.Model.TicketSelectionModel;
import com.result.bhutanking.Model.WeeklyTicketBuyingDetailsModel.DrawStatus;
import com.result.bhutanking.R;
import com.result.bhutanking.WeeklyTicketSelectionListener;

import java.util.List;


public class WeeklyBuyTicketAdapter extends RecyclerView.Adapter<WeeklyBuyTicketAdapter.ViewHolder> {

    private List<TicketSelectionModel> mData;
    private LayoutInflater mInflater;
    private Context mContext;
    private Activity baseActivity;
    private WeeklyTicketSelectionListener mListener;
    private int totalSelectedCount = 0;
    List<DrawStatus> alreadyBookedList;

    public void setDataSet(List<TicketSelectionModel> data) {
        this.mData = data;
    }


    // data is passed into the constructor
    public WeeklyBuyTicketAdapter(Context context, List<TicketSelectionModel> data, List<DrawStatus> alreadySelectedList, WeeklyTicketSelectionListener listener) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.mContext = context;
        this.baseActivity = (DashboardActivity) context;
        this.mListener = listener;
        this.alreadyBookedList = alreadySelectedList;
    }

    // inflates the row layout from xml when needed
    @NonNull
    @Override
    public WeeklyBuyTicketAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.weekly_ticket_item, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final TicketSelectionModel ticket = mData.get(position);

        holder.ticketNo.setText(ticket.getTicketNo());

        if (ticket.isSelected()) {
            holder.parent.setBackgroundColor(Color.parseColor("#039017"));
        } else {
            holder.parent.setBackgroundColor(Color.parseColor("#DA7C09"));
        }

        for (int i = 0; i < alreadyBookedList.size(); i++) {
            if (ticket.getTicketNo().equalsIgnoreCase(alreadyBookedList.get(i).getTicket_no())) {
                holder.parent.setBackgroundColor(Color.parseColor("#860306"));
                holder.parent.setEnabled(false);
            }
        }

        holder.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ticket.isSelected()) {
                    if (totalSelectedCount > 0) {
                        totalSelectedCount--;
                        mListener.ticketSelectedOrRemoved(ticket, false, false);
                        ticket.setSelected(false);
                        holder.parent.setBackgroundColor(Color.parseColor("#DA7C09"));
                    }
                } else {
                    if (totalSelectedCount < 10) {
                        totalSelectedCount++;
                        mListener.ticketSelectedOrRemoved(ticket, true, false);
                        ticket.setSelected(true);
                        holder.parent.setBackgroundColor(Color.parseColor("#039017"));
                    }
                    else {
                        mListener.ticketSelectedOrRemoved(null, false, true);
                    }
                }
            }
        });

    }


    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView ticketNo;
        LinearLayout parent;

        ViewHolder(View itemView) {
            super(itemView);
            ticketNo = itemView.findViewById(R.id.ticket_id);
            parent = itemView.findViewById(R.id.ticketLayout);
        }

    }


    // convenience method for getting data at click position
    TicketSelectionModel getItem(int id) {
        return mData.get(id);
    }

}
