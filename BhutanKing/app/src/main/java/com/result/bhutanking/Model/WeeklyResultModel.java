package com.result.bhutanking.Model;

import java.util.HashMap;
import java.util.List;

public class WeeklyResultModel {

    private List<Response> response;

    public List<Response> getResponse() {
        return response;
    }

    public void setResponse(List<Response> response) {
        this.response = response;
    }


    public class Response
    {
        private String status;
        private String message;
//        private WinningNos winning_nos;
        private HashMap<String, List<String>> winning_nos;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public HashMap<String, List<String>> getWinning_nos() {
            return winning_nos;
        }

        public void setWinning_nos(HashMap<String, List<String>> winning_nos) {
            this.winning_nos = winning_nos;
        }

//        public WinningNos getWinning_nos() {
//            return winning_nos;
//        }
//
//        public void setWinning_nos(WinningNos winning_nos) {
//            this.winning_nos = winning_nos;
//        }
    }

}
