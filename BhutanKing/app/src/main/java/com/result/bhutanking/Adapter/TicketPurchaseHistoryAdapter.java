package com.result.bhutanking.Adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.result.bhutanking.DashboardActivity;
import com.result.bhutanking.Model.HistoryModel;
import com.result.bhutanking.R;

import java.util.List;


public class TicketPurchaseHistoryAdapter extends RecyclerView.Adapter<TicketPurchaseHistoryAdapter.ViewHolder>  {

    private List<HistoryModel.Ticket_Purchase_History> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context mContext;
    private Activity baseActivity;


    // data is passed into the constructor
    public TicketPurchaseHistoryAdapter(Context context, List<HistoryModel.Ticket_Purchase_History> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.mContext = context;
        this.baseActivity = (DashboardActivity)context;
    }

    // inflates the row layout from xml when needed
    @NonNull
    @Override
    public TicketPurchaseHistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.ticket_purchase_history_item, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final HistoryModel.Ticket_Purchase_History transactions = mData.get(position);
        if (transactions.getPurchase_date() != null && !TextUtils.isEmpty(transactions.getPurchase_date().trim())) {
            holder.date.setText(transactions.getPurchase_date().split(" ")[0]);
        }
        else {
            holder.date.setText("");
        }
        holder.board.setText(transactions.getLottery_type());
        holder.noOfTicket.setText(String.valueOf(transactions.getTicket_count()));
        int ticketPositionCount = transactions.getTicket_no_position().length();
        String ticket_no = "";
        /*for (int i = 0; i < ticketPositionCount; i++)
        {
            if (i != 0){
                ticket_no.append("-");
            }
            ticket_no.append(transactions.getTicket_no_position().charAt(i)).append(transactions.getTicket_number().charAt(i));
        }*/

        ticket_no = transactions.getTicket_no_position()+"-"+transactions.getTicket_number();
        if (ticketPositionCount >= 3)
        {
            ticket_no = transactions.getTicket_number();
        }
        holder.ticketNo.setText(ticket_no);
        holder.amount.setText("₹ " + String.valueOf(transactions.getAmount()));
        if (position % 2 != 0)
        {
            holder.linearLayout.setBackgroundColor(Color.parseColor("#DDDDDD"));
        }
        else {
            holder.linearLayout.setBackgroundColor(Color.parseColor("#ECECEC"));
        }
    }


    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView date;
        TextView board;
        TextView ticketNo;
        TextView noOfTicket;
        TextView amount;
        LinearLayout linearLayout;

        ViewHolder(View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.ticketPurchaseDate);
            board = itemView.findViewById(R.id.ticketPurchaseBoard);
            noOfTicket = itemView.findViewById(R.id.ticketPurchaseCount);
            ticketNo = itemView.findViewById(R.id.ticketPurchaseNo);
            amount = itemView.findViewById(R.id.ticketPurchaseAmount);
            linearLayout = itemView.findViewById(R.id.linearLayout1);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }


    // convenience method for getting data at click position
    HistoryModel.Ticket_Purchase_History getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
