package com.result.bhutanking.Model;

import java.util.List;

public class HistoryModel {

    private List<Response> response;

    public List<Response> getResponse() {
        return response;
    }

    public void setResponse(List<Response> response) {
        this.response = response;
    }


    public class Response
    {
        private String status;
        private String message;
        private List<List<Transactions>> transactions;
        private List<List<Ticket_Purchase_History>> ticket_purchase_history;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<List<Ticket_Purchase_History>> getTicket_purchase_history() {
            return ticket_purchase_history;
        }

        public void setTicket_purchase_history(List<List<Ticket_Purchase_History>> ticket_purchase_history) {
            this.ticket_purchase_history = ticket_purchase_history;
        }

        public List<List<Transactions>> getTransactions() {
            return transactions;
        }

        public void setTransactions(List<List<Transactions>> transactions) {
            this.transactions = transactions;
        }
    }

    public class Transactions
    {
        private String type;
        private String description;
        private String ticket_price;
        private String ticket_qty;
        private int amount;
        private String transaction_datetime;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getTicket_price() {
            return ticket_price;
        }

        public void setTicket_price(String ticket_price) {
            this.ticket_price = ticket_price;
        }

        public String getTicket_qty() {
            return ticket_qty;
        }

        public void setTicket_qty(String ticket_qty) {
            this.ticket_qty = ticket_qty;
        }

        public int getAmount() {
            return amount;
        }

        public void setAmount(int amount) {
            this.amount = amount;
        }

        public String getTransaction_datetime() {
            return transaction_datetime;
        }

        public void setTransaction_datetime(String transaction_datetime) {
            this.transaction_datetime = transaction_datetime;
        }
    }

    public class Ticket_Purchase_History
    {
        private String lottery_type;
        private String ticket_number;
        private int ticket_count;
        private int number_of_digits;
        private int amount;
        private String ticket_no_position;
        private String purchase_date;

        public String getLottery_type() {
            return lottery_type;
        }

        public void setLottery_type(String lottery_type) {
            this.lottery_type = lottery_type;
        }

        public String getTicket_number() {
            return ticket_number;
        }

        public void setTicket_number(String ticket_number) {
            this.ticket_number = ticket_number;
        }

        public int getTicket_count() {
            return ticket_count;
        }

        public void setTicket_count(int ticket_count) {
            this.ticket_count = ticket_count;
        }

        public int getNumber_of_digits() {
            return number_of_digits;
        }

        public void setNumber_of_digits(int number_of_digits) {
            this.number_of_digits = number_of_digits;
        }

        public int getAmount() {
            return amount;
        }

        public void setAmount(int amount) {
            this.amount = amount;
        }

        public String getTicket_no_position() {
            return ticket_no_position;
        }

        public void setTicket_no_position(String ticket_no_position) {
            this.ticket_no_position = ticket_no_position;
        }

        public String getPurchase_date() {
            return purchase_date;
        }

        public void setPurchase_date(String purchase_date) {
            this.purchase_date = purchase_date;
        }
    }
}
