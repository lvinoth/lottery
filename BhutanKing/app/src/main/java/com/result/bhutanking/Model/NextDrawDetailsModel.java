package com.result.bhutanking.Model;

import java.util.List;

public class NextDrawDetailsModel {

    private List<Response> response;

    public List<Response> getResponse() {
        return response;
    }

    public void setResponse(List<Response> response) {
        this.response = response;
    }


    public class Response
    {
        private String status;
        private String message;
        private int available_balance;

        private List<Time_Slots> time_slots;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }


        public List<Time_Slots> getTime_slots() {
            return time_slots;
        }

        public void setTime_slots(List<Time_Slots> time_slots) {
            this.time_slots = time_slots;
        }

        public int getAvailable_balance() {
            return available_balance;
        }

        public void setAvailable_balance(int available_balance) {
            this.available_balance = available_balance;
        }
    }

    public class Time_Slots
    {
        private String next_time_slot;
        private String category;


        public String getNext_time_slot() {
            return next_time_slot;
        }

        public void setNext_time_slot(String next_time_slot) {
            this.next_time_slot = next_time_slot;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }
    }

}
