package com.result.bhutanking;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.text.method.SingleLineTransformationMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.result.bhutanking.Model.GeneralModel;
import com.result.bhutanking.Model.ProfileModel;
import com.result.bhutanking.Retrofit.ApiClient;
import com.result.bhutanking.Retrofit.ApiInterface;
import com.result.bhutanking.Utils.SharedPreferenceUtility;
import com.result.bhutanking.Utils.Utils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.widget.Toast.LENGTH_LONG;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = ProfileFragment.class.getSimpleName();
    private Activity baseActivity;
    private Toast mToast;
    private ApiInterface apiService;
    private Button updateUserDetailsBtn;
    private Button updateBankDetailsBtn;
    private TextView userIdTxt;
    private TextView userNameTxt;
    private TextView userMobileTxt;
    private TextView userEmailTxt;
    private EditText userChangePasswordTxt;
    private EditText userConfirmPasswordTxt;

    private EditText bankNameTxt;
    private EditText bankAccNoTxt;
    private EditText bankAccNameTxt;
    private EditText bankIfscTxt;
    private EditText bankUpiTxt;
    private ImageView logoutBtn;

    ImageView show_hide_password;
    ImageView show_hide_confirm_password;


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public ProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProfileFragment newInstance(String param1, String param2) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root =  inflater.inflate(R.layout.fragment_profile, container, false);
        baseActivity = getActivity();
        apiService = ApiClient.getClient().create(ApiInterface .class);
        userIdTxt = root.findViewById(R.id.user_id_txt);
        userNameTxt = root.findViewById(R.id.user_name_txt);
        userMobileTxt = root.findViewById(R.id.user_mobile_txt);
        userEmailTxt = root.findViewById(R.id.user_email_txt);
        logoutBtn = root.findViewById(R.id.logoutBtn);
        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               confirmLogout();
            }
        });
        show_hide_password = root.findViewById(R.id.show_hide_password);
        show_hide_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userChangePasswordTxt.getTransformationMethod().getClass().getSimpleName() .equals("PasswordTransformationMethod")) {
                    userChangePasswordTxt.setTransformationMethod(new SingleLineTransformationMethod());
                    show_hide_password.setImageDrawable(getActivity().getDrawable(R.drawable.ic_hide_eye));
                }
                else {
                    show_hide_password.setImageDrawable(getActivity().getDrawable(R.drawable.ic_show_eye));
                    userChangePasswordTxt.setTransformationMethod(new PasswordTransformationMethod());
                }

                userChangePasswordTxt.setSelection(userChangePasswordTxt.getText().length());
            }
        });
        show_hide_confirm_password = root.findViewById(R.id.show_hide_confirm_password);
        show_hide_confirm_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userConfirmPasswordTxt.getTransformationMethod().getClass().getSimpleName() .equals("PasswordTransformationMethod")) {
                    userConfirmPasswordTxt.setTransformationMethod(new SingleLineTransformationMethod());
                    show_hide_confirm_password.setImageDrawable(getActivity().getDrawable(R.drawable.ic_hide_eye));
                }
                else {
                    show_hide_confirm_password.setImageDrawable(getActivity().getDrawable(R.drawable.ic_show_eye));
                    userConfirmPasswordTxt.setTransformationMethod(new PasswordTransformationMethod());
                }

                userConfirmPasswordTxt.setSelection(userConfirmPasswordTxt.getText().length());
            }
        });
        userChangePasswordTxt = root.findViewById(R.id.user_change_password_txt);
        userConfirmPasswordTxt = root.findViewById(R.id.user_confirm_password_txt);
        userConfirmPasswordTxt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    updateUserDetailsBtn.performClick();
                    return true;
                }
                return false;
            }
        });
        updateUserDetailsBtn = root.findViewById(R.id.update_user_details);
        updateUserDetailsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            String password = userChangePasswordTxt.getText().toString().trim();
            String confirm_password = userConfirmPasswordTxt.getText().toString().trim();
            if (TextUtils.isEmpty(password)) {
                    showToastAtCentre("New Password should not be empty",LENGTH_LONG);
                    return;
                }
                if (TextUtils.isEmpty(confirm_password)) {
                    showToastAtCentre("Confirm Password should not be empty",LENGTH_LONG);
                    return;
                }

                if (!password.equals(confirm_password)) {
                    showToastAtCentre("Password doesn't match",LENGTH_LONG);
                    return;
                }
                updateUserDetails(password, confirm_password);
            }
        });

        updateBankDetailsBtn = root.findViewById(R.id.update_bank_details);
        updateBankDetailsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String _bankName = bankNameTxt.getText().toString().trim();
                String _bankAccHolderName = bankAccNameTxt.getText().toString().trim();
                String _bankAccNum = bankAccNoTxt.getText().toString().trim();
                String _bankAccIfsc = bankIfscTxt.getText().toString().trim();
                String _bankAccUpi = bankUpiTxt.getText().toString().trim();

                updateBankDetails(_bankName, _bankAccHolderName, _bankAccNum, "",_bankAccIfsc, _bankAccUpi);
            }
        });

        bankNameTxt = root.findViewById(R.id.bank_name_txt);
        bankAccNoTxt = root.findViewById(R.id.bank_acc_No_txt);
        bankAccNameTxt = root.findViewById(R.id.bank_acc_name_txt);
        bankIfscTxt = root.findViewById(R.id.bank_ifsc_txt);
        bankUpiTxt = root.findViewById(R.id.bank_upi_txt);
        bankUpiTxt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    updateBankDetailsBtn.performClick();
                    return true;
                }
                return false;
            }
        });
        getProfileDetails();
        return root;
    }

    private void confirmLogout() {
        final Dialog dialog = new Dialog(baseActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_alert);

        WindowManager.LayoutParams wlp = dialog.getWindow().getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_FULLSCREEN;
        dialog.getWindow().setAttributes(wlp);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
        TextView desc = dialog.findViewById(R.id.alertText);
        TextView cancelBtn = dialog.findViewById(R.id.alertCancel);
        TextView okBtn = dialog.findViewById(R.id.alertOk);

        desc.setText(R.string.logout);

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Invoke Delete API here
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);
                SharedPreferenceUtility.clearUserDetails(getActivity());
                getActivity().finish();
                dialog.cancel();
            }
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
    }

    private void updateUserDetails(String password, String confirm_password) {

        Utils.showBusyAnimation(baseActivity,"Authenticating..");
        String token = SharedPreferenceUtility.getUserToken(getActivity());

        Call<GeneralModel> call = apiService.updateUserDetails(token,"user_update",password);

        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {

                Utils.hideBusyAnimation(baseActivity);
                if(response.body() != null) {
                    if (response.body().getResponse().get(0).getStatus().equalsIgnoreCase("success"))
                    {
                        showToastAtCentre("Password updated successfully!",LENGTH_LONG);
                    }
                    else // Handle failure cases here
                    {
                        String mess = response.body().getResponse().get(0).getMessage();
                        Log.d(TAG, "Response: " + mess);
                        showToastAtCentre("" + mess, LENGTH_LONG);
                    }
                }

            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                Utils.hideBusyAnimation(baseActivity);
                Log.d(TAG, "Response: " + t.getMessage());
                if (t instanceof IOException) {
                    Toast.makeText(baseActivity, "No Internet Connection!!", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void updateBankDetails(String bankName, String holderName, String accNum, String accType, String ifscCode, String upiCode) {

        Utils.showBusyAnimation(baseActivity,"Authenticating..");
        String token = SharedPreferenceUtility.getUserToken(getActivity());

        Call<GeneralModel> call = apiService.updateBankDetails(token,"bank_update",bankName,holderName,accNum,accType,ifscCode,upiCode);

        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {

                Utils.hideBusyAnimation(baseActivity);
                if(response.body() != null) {
                    if (response.body().getResponse().get(0).getStatus().equalsIgnoreCase("success"))
                    {
                        showToastAtCentre("Bank details updated successfully!",LENGTH_LONG);
                        ((DashboardActivity)getActivity()).replaceFragments("home","");
                    }
                    else // Handle failure cases here
                    {
                        String mess = response.body().getResponse().get(0).getMessage();
                        Log.d(TAG, "Response: " + mess);
                        showToastAtCentre("" + mess, LENGTH_LONG);
                    }
                }

            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                Utils.hideBusyAnimation(baseActivity);
                Log.d(TAG, "Response: " + t.getMessage());
                if (t instanceof IOException) {
                    Toast.makeText(baseActivity, "No Internet Connection!!", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void getProfileDetails()
    {
        Utils.showBusyAnimation(baseActivity,"Authenticating..");
        String token = SharedPreferenceUtility.getUserToken(getActivity());

        Call<ProfileModel> call = apiService.getProfileDetails(token,"profile");

        call.enqueue(new Callback<ProfileModel>() {
            @Override
            public void onResponse(Call<ProfileModel> call, Response<ProfileModel> response) {

                Utils.hideBusyAnimation(baseActivity);
                if(response.body() != null) {
                    if (response.body().getResponse().get(0).getStatus().equalsIgnoreCase("success"))
                    {
                        String userName = response.body().getResponse().get(0).getUser().getName();
                        String userEmail = response.body().getResponse().get(0).getUser().getEmail();
                        String userMobile = response.body().getResponse().get(0).getUser().getMobile();
                        String userRegId = response.body().getResponse().get(0).getUser().getReg_id();
                        int balance = response.body().getResponse().get(0).getUser().getAvailable_balance();

                        userIdTxt.setText(": "+ userRegId);
                        userNameTxt.setText(": "+ userName);
                        userMobileTxt.setText(": "+ userMobile);
                        userEmailTxt.setText(": "+ userEmail);

                        String bankName = response.body().getResponse().get(0).getBank().getBank_name();
                        String acc_holderName = response.body().getResponse().get(0).getBank().getAcc_holder_name();
                        String bankAccNo = response.body().getResponse().get(0).getBank().getAcc_number();
                        String bankAccType = response.body().getResponse().get(0).getBank().getAcc_type();
                        String bankIfsc = response.body().getResponse().get(0).getBank().getIfsc_code();
                        String bankUpi = response.body().getResponse().get(0).getBank().getUpi_code();

                        bankNameTxt.setText(bankName);
                        bankAccNameTxt.setText(acc_holderName);
                        bankAccNoTxt.setText(bankAccNo);
                        bankIfscTxt.setText(bankIfsc);
                        bankUpiTxt.setText(bankUpi);

                    }
                    else // Handle failure cases here
                    {
                        String mess = response.body().getResponse().get(0).getMessage();
                        Log.d(TAG, "Response: " + mess);
                        showToastAtCentre("" + mess, LENGTH_LONG);
                    }
                }

            }

            @Override
            public void onFailure(Call<ProfileModel> call, Throwable t) {
                Utils.hideBusyAnimation(baseActivity);
                Log.d(TAG, "Response: " + t.getMessage());
                if (t instanceof IOException) {
                    Toast.makeText(baseActivity, "No Internet Connection!!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void showToastAtCentre(String message, int duration) {
        if (mToast != null) {
            mToast.cancel();
            mToast = null;
        }
        mToast = Toast.makeText(getActivity().getApplicationContext(), message, duration);
        mToast.show();
    }
}
