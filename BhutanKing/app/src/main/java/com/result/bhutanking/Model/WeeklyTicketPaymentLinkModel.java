package com.result.bhutanking.Model;

import java.util.List;

public class WeeklyTicketPaymentLinkModel {

    private List<Response> response;

    public List<Response> getResponse() {
        return response;
    }

    public void setResponse(List<Response> response) {
        this.response = response;
    }


    public class Response
    {
        private String status;
        private String message;
        private List<List<Link>> weekly_draw;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<List<Link>> getWeekly_draw() {
            return weekly_draw;
        }

        public void setWeekly_draw(List<List<Link>> weekly_draw) {
            this.weekly_draw = weekly_draw;
        }
    }

    public class LinkCat
    {
        private List<List<Link>> weekly_draw;

        public List<List<Link>> getWeekly_draw() {
            return weekly_draw;
        }

        public void setWeekly_draw(List<List<Link>> weekly_draw) {
            this.weekly_draw = weekly_draw;
        }
    }


    public class Link
    {
        private String type;
        private String amount;
        private String link;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }
    }

}
