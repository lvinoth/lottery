package com.result.bhutanking;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.result.bhutanking.Model.AddCashModel;
import com.result.bhutanking.Model.AgentBankDetailsModel;
import com.result.bhutanking.Retrofit.ApiClient;
import com.result.bhutanking.Retrofit.ApiInterface;
import com.result.bhutanking.Utils.SharedPreferenceUtility;
import com.result.bhutanking.Utils.Utils;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.widget.Toast.LENGTH_LONG;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AddCashFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddCashFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = AddCashFragment.class.getSimpleName();
    private ImageView cash_2000;
    private ImageView cash_500;
    private ImageView cash_300;
    private ImageView cash_100;
    private ImageView cash_1000;
    private ImageView qrCodeImg;
    private Activity baseActivity;
    private Toast mToast;

    private LinearLayout layout1;
    private LinearLayout layout2;
    private LinearLayout bankLayout;
    private TextView bankNameTxt;
    private TextView accNameTxt;
    private TextView accNoTxt;
    private TextView ifscCodeTxt;
    private TextView upiCodeTxt;
    private ApiInterface apiService;
    List<AddCashModel.Add_Cash> addCashLinkList = new ArrayList<>();

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public AddCashFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AddCashFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AddCashFragment newInstance(String param1, String param2) {
        AddCashFragment fragment = new AddCashFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View root =  inflater.inflate(R.layout.fragment_add_cash, container, false);
        baseActivity = getActivity();
        apiService = ApiClient.getClient().create(ApiInterface .class);
        layout1 = root.findViewById(R.id.cashLayout1);
        layout2 = root.findViewById(R.id.cashLayout2);
        bankLayout = root.findViewById(R.id.bankLayout);
        bankNameTxt = root.findViewById(R.id.bank_name_txt);
        accNameTxt = root.findViewById(R.id.bank_acc_name_txt);
        accNoTxt = root.findViewById(R.id.bank_acc_No_txt);
        ifscCodeTxt = root.findViewById(R.id.bank_ifsc_txt);
        upiCodeTxt = root.findViewById(R.id.bank_upi_txt);

        qrCodeImg = root.findViewById(R.id.qrCodeImg);
        cash_100 = root.findViewById(R.id.cash_100);
        cash_300 = root.findViewById(R.id.cash_300);
        cash_500 = root.findViewById(R.id.cash_500);
        cash_1000 = root.findViewById(R.id.cash_1000);
        cash_2000 = root.findViewById(R.id.addcash_2000);
        cash_2000.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startPayment("2000");
            }
        });
        cash_1000.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startPayment("1000");
            }
        });
        cash_500.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startPayment("500");
            }
        });
        cash_300.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startPayment("300");
            }
        });
        cash_100.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startPayment("100");
            }
        });
        getAgentBankDetails();
        return root;
    }

    private void startPayment(String amount)
    {
        Intent i = new Intent(getActivity(), PaymentActivity.class);
        for (AddCashModel.Add_Cash link: addCashLinkList) {
            if (link.getAmount().contains(amount)){
                String payLink = link.getLink();
                i.putExtra("amount",payLink);
            }
        }
        startActivity(i);
    }


    private void getAddCashDetails()
    {
        Utils.showBusyAnimation(baseActivity,"Authenticating..");
        String token = SharedPreferenceUtility.getUserToken(getActivity());

        Call<AddCashModel> call = apiService.getAddCashLinks(token,"add_cash_links");

        call.enqueue(new Callback<AddCashModel>() {
            @Override
            public void onResponse(Call<AddCashModel> call, Response<AddCashModel> response) {

                Utils.hideBusyAnimation(baseActivity);
                if(response.body() != null) {
                    if (response.body().getResponse().get(0).getStatus().equalsIgnoreCase("success"))
                    {
                        for (List<AddCashModel.Add_Cash> link: response.body().getResponse().get(0).getAdd_cash()) {
                            addCashLinkList.add(link.get(0));
                        }
                    }
                    else // Handle failure cases here
                    {
                        String mess = response.body().getResponse().get(0).getMessage();
                        Log.d(TAG, "Response: " + mess);
                        showToastAtCentre("" + mess, LENGTH_LONG);
                    }
                }

            }

            @Override
            public void onFailure(Call<AddCashModel> call, Throwable t) {
                Utils.hideBusyAnimation(baseActivity);
                Log.d(TAG, "Response: " + t.getMessage());
                if (t instanceof IOException) {
                    Toast.makeText(baseActivity, "No Internet Connection!!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void getAgentBankDetails()
    {
        Utils.showBusyAnimation(baseActivity,"Authenticating..");
        String token = SharedPreferenceUtility.getUserToken(getActivity());

        Call<AgentBankDetailsModel> call = apiService.getAgentBankDetails(token,"bk_acc_details");

        call.enqueue(new Callback<AgentBankDetailsModel>() {
            @Override
            public void onResponse(Call<AgentBankDetailsModel> call, Response<AgentBankDetailsModel> response) {

                Utils.hideBusyAnimation(baseActivity);
                if(response.body() != null) {
                    if (response.body().getResponse().get(0).getStatus().equalsIgnoreCase("success"))
                    {
                        if (response.body().getResponse().get(0).getAgent().equalsIgnoreCase("no"))
                        {
                            layout1.setVisibility(View.VISIBLE);
                            layout2.setVisibility(View.VISIBLE);
                            bankLayout.setVisibility(View.GONE);
                            getAddCashDetails();
                        }
                        else {
                            layout1.setVisibility(View.GONE);
                            layout2.setVisibility(View.GONE);
                            bankLayout.setVisibility(View.VISIBLE);
                            bankNameTxt.setText(response.body().getResponse().get(0).getBank_name().trim());
                            accNameTxt.setText(response.body().getResponse().get(0).getAccount_name().trim());
                            accNoTxt.setText(response.body().getResponse().get(0).getAccount_number().trim());
                            ifscCodeTxt.setText(response.body().getResponse().get(0).getIfsc_code().trim());
                            upiCodeTxt.setText(response.body().getResponse().get(0).getUpi_code().trim());
                            Picasso.get().load(response.body().getResponse().get(0).getQr_code())
                            .noFade()
                            .into(qrCodeImg);
                        }
                    }
                    else // Handle failure cases here
                    {
                        String mess = response.body().getResponse().get(0).getMessage();
                        Log.d(TAG, "Response: " + mess);
                        showToastAtCentre("" + mess, LENGTH_LONG);
                    }
                }

            }

            @Override
            public void onFailure(Call<AgentBankDetailsModel> call, Throwable t) {
                Utils.hideBusyAnimation(baseActivity);
                Log.d(TAG, "Response: " + t.getMessage());
                if (t instanceof IOException) {
                    Toast.makeText(baseActivity, "No Internet Connection!!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void showToastAtCentre(String message, int duration) {
        if (mToast != null) {
            mToast.cancel();
            mToast = null;
        }
        mToast = Toast.makeText(getActivity().getApplicationContext(), message, duration);
        mToast.show();
    }
}
