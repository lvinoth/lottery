package com.result.bhutanking;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.text.method.SingleLineTransformationMethod;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.result.bhutanking.Model.LoginModel;
import com.result.bhutanking.Retrofit.ApiClient;
import com.result.bhutanking.Retrofit.ApiInterface;
import com.result.bhutanking.Utils.SharedPreferenceUtility;
import com.result.bhutanking.Utils.Utils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.widget.Toast.LENGTH_LONG;

public class RegistrationActivity extends AppCompatActivity {

    private EditText nameEditText;
    private EditText mobileEditText;
    private EditText emailEditText;
    private EditText passwordEditText;
    private EditText confirmPasswordEditText;
    private Toast mToast;
    private Activity baseActivity;
    private ApiInterface apiService;
    ImageView show_hide_password;
    ImageView show_hide_confirm_password;

    private Button registerButton;
    private final String TAG = RegistrationActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.makeStatusBarTransparent(getWindow(),this);
        setContentView(R.layout.activity_registration);
        baseActivity = this;
        apiService = ApiClient.getClient().create(ApiInterface.class);
        nameEditText = findViewById(R.id.nameTxt);
        mobileEditText = findViewById(R.id.mobTxt);
        emailEditText = findViewById(R.id.emailTxt);
        show_hide_password = findViewById(R.id.show_hide_password);
        show_hide_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (passwordEditText.getTransformationMethod().getClass().getSimpleName() .equals("PasswordTransformationMethod")) {
                    passwordEditText.setTransformationMethod(new SingleLineTransformationMethod());
                    show_hide_password.setImageDrawable(getDrawable(R.drawable.ic_hide_eye));
                }
                else {
                    show_hide_password.setImageDrawable(getDrawable(R.drawable.ic_show_eye));
                    passwordEditText.setTransformationMethod(new PasswordTransformationMethod());
                }

                passwordEditText.setSelection(passwordEditText.getText().length());
            }
        });
        show_hide_confirm_password = findViewById(R.id.show_hide_confirm_password);
        show_hide_confirm_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (confirmPasswordEditText.getTransformationMethod().getClass().getSimpleName() .equals("PasswordTransformationMethod")) {
                    confirmPasswordEditText.setTransformationMethod(new SingleLineTransformationMethod());
                    show_hide_confirm_password.setImageDrawable(getDrawable(R.drawable.ic_hide_eye));
                }
                else {
                    show_hide_confirm_password.setImageDrawable(getDrawable(R.drawable.ic_show_eye));
                    confirmPasswordEditText.setTransformationMethod(new PasswordTransformationMethod());
                }

                confirmPasswordEditText.setSelection(confirmPasswordEditText.getText().length());
            }
        });
        passwordEditText = findViewById(R.id.passwordTxt);
        confirmPasswordEditText = findViewById(R.id.confirmPasswordTxt);
        confirmPasswordEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    registerButton.performClick();
                    return true;
                }
                return false;
            }
        });

        registerButton  =findViewById(R.id.registerBtn);

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = nameEditText.getText().toString().trim();
                String mobile = mobileEditText.getText().toString().trim();
                String email = emailEditText.getText().toString().trim();
                String password = passwordEditText.getText().toString().trim();
                String confirmPassword = confirmPasswordEditText.getText().toString().trim();
                if(TextUtils.isEmpty(name))
                {
                    showToastAtCentre("Please enter your Name",LENGTH_LONG);
                    return;
                }
                if(TextUtils.isEmpty(mobile))
                {
                    showToastAtCentre("Please enter the Mobile number",LENGTH_LONG);
                    return;
                }
                if(TextUtils.isEmpty(email))
                {
                    showToastAtCentre("Please enter the Email",LENGTH_LONG);
                    return;
                }
                if (!isValidEmail(email))
                {
                    showToastAtCentre("Please enter a valid Email",LENGTH_LONG);
                    return;
                }
                if(TextUtils.isEmpty(password))
                {
                    showToastAtCentre("Please enter the Password",LENGTH_LONG);
                    return;
                }
                if(TextUtils.isEmpty(confirmPassword))
                {
                    showToastAtCentre("Please enter the confirmation password",LENGTH_LONG);
                    return;
                }
                if (!password.equals(confirmPassword))
                {
                    showToastAtCentre("Password doesn't match",LENGTH_LONG);
                    return;
                }
                    doRegister(name, mobile, email, password);
            }
        });
    }

    public boolean isValidEmail(String target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    private void doRegister(String name, String mobile, String email, String password) {
        Utils.showBusyAnimation(baseActivity,"Authenticating..");

        Call<LoginModel> call = apiService.registerUser(email,password,"register",name, mobile);

        call.enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {

                Utils.hideBusyAnimation(baseActivity);
                if(response.body() != null) {
                    if (response.body().getResponse().get(0).getStatus().equalsIgnoreCase("success"))
                    {
                        Intent intent = new Intent(RegistrationActivity.this,DashboardActivity.class);
                        intent.putExtra("source","Register");
                        // Save the token in prefs
                        String userName =response.body().getResponse().get(0).getName();
                        String token =response.body().getResponse().get(0).getToken();
                        String regId =response.body().getResponse().get(0).getReg_id();
                        int balance =response.body().getResponse().get(0).getWallet_amt();
                        SharedPreferenceUtility.saveUserToken(getApplicationContext(),userName, token, regId, balance);
                        startActivity(intent);
                        finish();
                    }
                    else // Handle failure cases here
                    {
                        String mess = response.body().getResponse().get(0).getMessage();
                        Log.d(TAG, "Response: " + mess);
                        showToastAtCentre("" + mess, LENGTH_LONG);
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {
                Utils.hideBusyAnimation(baseActivity);
                Log.d(TAG, "Response: " + t.getMessage());
                if (t instanceof IOException) {
                    Toast.makeText(baseActivity, "No Internet Connection!!", Toast.LENGTH_SHORT).show();
                }
            }
        });
//        showToastAtCentre("Registration Successful",LENGTH_SHORT);
    }


    private void showToastAtCentre(String message, int duration) {
        if (mToast != null) {
            mToast.cancel();
            mToast = null;
        }
        mToast = Toast.makeText(getApplicationContext(), message, duration);
        mToast.show();
    }
}
