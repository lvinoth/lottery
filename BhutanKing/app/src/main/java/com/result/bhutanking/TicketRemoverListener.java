package com.result.bhutanking;

public interface TicketRemoverListener {
    void removeTicket(int position);

}
