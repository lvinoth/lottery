package com.result.bhutanking;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.text.method.SingleLineTransformationMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.result.bhutanking.Model.GeneralModel;
import com.result.bhutanking.Model.LoginModel;
import com.result.bhutanking.Retrofit.ApiClient;
import com.result.bhutanking.Retrofit.ApiInterface;
import com.result.bhutanking.Utils.SharedPreferenceUtility;
import com.result.bhutanking.Utils.Utils;


import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.widget.Toast.LENGTH_LONG;
import static android.widget.Toast.LENGTH_SHORT;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = LoginActivity.class.getSimpleName();
    Button loginBtn;
    TextView registerTxt;
    TextView forgotPasswordTxt;
    private Activity baseActivity;
    private Toast mToast;
    private ApiInterface apiService;
    EditText usernameTxt;
    EditText passwordTxt;
    ImageView show_hide_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.makeStatusBarTransparent(getWindow(),this);
        setContentView(R.layout.activity_login);
        baseActivity = this;
        apiService = ApiClient.getClient().create(ApiInterface.class);
        usernameTxt = findViewById(R.id.userNameTxt);
        show_hide_password = findViewById(R.id.show_hide_password);
        show_hide_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (passwordTxt.getTransformationMethod().getClass().getSimpleName() .equals("PasswordTransformationMethod")) {
                    passwordTxt.setTransformationMethod(new SingleLineTransformationMethod());
                    show_hide_password.setImageDrawable(getDrawable(R.drawable.ic_hide_eye));
                }
                else {
                    show_hide_password.setImageDrawable(getDrawable(R.drawable.ic_show_eye));
                    passwordTxt.setTransformationMethod(new PasswordTransformationMethod());
                }

                passwordTxt.setSelection(passwordTxt.getText().length());
            }
        });
        passwordTxt = findViewById(R.id.passwordTxt);
        passwordTxt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    loginBtn.performClick();
                    return true;
                }
                return false;
            }
        });
        forgotPasswordTxt = findViewById(R.id.forgotPassword);
        forgotPasswordTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPassword();
            }
        });
        loginBtn = findViewById(R.id.loginBtn);
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userName = usernameTxt.getText().toString();
                String password = passwordTxt.getText().toString();

                if (userName.length() == 0)
                {
                    showToastAtCentre("Please enter your Email/ Mobile number", LENGTH_LONG);
                    return;
                }
                if (password.length() == 0)
                {
                    showToastAtCentre("Please enter the Password", LENGTH_LONG);
                    return;
                }

                authenticate_user(userName, password);
            }
        });
        registerTxt = findViewById(R.id.registerBtn);
        registerTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this,RegistrationActivity.class);
                startActivity(intent);
            }
        });
        registerReceiver(broadcastReceiver,new IntentFilter("finish_activity"));
    }

    public void authenticate_user(final String usrName, final String passwrd)
    {
        Utils.showBusyAnimation(baseActivity,"Authenticating..");

        Call<LoginModel> call = apiService.authenticateLogin(usrName,passwrd,"login");

        call.enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {

                Utils.hideBusyAnimation(baseActivity);
                if(response.body() != null) {
                    if (response.body().getResponse().get(0).getStatus().equalsIgnoreCase("success"))
                    {
                        Intent intent = new Intent(LoginActivity.this,DashboardActivity.class);
                        // Save the token in prefs
                        String userName =response.body().getResponse().get(0).getName();
                        String token =response.body().getResponse().get(0).getToken();
                        String regId =response.body().getResponse().get(0).getReg_id();
                        int balance =response.body().getResponse().get(0).getWallet_amt();
                        SharedPreferenceUtility.saveUserToken(getApplicationContext(),userName, token, regId, balance);
                        startActivity(intent);
                        finish();
                    }
                    else // Handle failure cases here
                    {
                        String mess = response.body().getResponse().get(0).getMessage();
                        Log.d(TAG, "Response: " + mess);
                        showToastAtCentre("" + mess, LENGTH_LONG);
                    }
                }

            }

            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {
                Utils.hideBusyAnimation(baseActivity);
                Log.d(TAG, "Response: " + t.getMessage());
                if (t instanceof IOException) {
                    Toast.makeText(baseActivity, "No Internet Connection!!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context arg0, Intent intent) {
            String action = intent.getAction();
            if (action.equals("finish_activity")) {

                Log.d(LoginActivity.class.getSimpleName(),"Broadcast Received!!!!");
                finish();
            }
        }
    };

    private void getPassword() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.password_alert);

        WindowManager.LayoutParams wlp = dialog.getWindow().getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_FULLSCREEN;
        dialog.getWindow().setAttributes(wlp);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
        TextView desc = dialog.findViewById(R.id.alertText);
        final EditText email = dialog.findViewById(R.id.forgotEmail);
        TextView cancelBtn = dialog.findViewById(R.id.alertCancel);
        TextView okBtn = dialog.findViewById(R.id.alertOk);

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Invoke Delete API here
                if (TextUtils.isEmpty(email.getText().toString().trim()))
                {
                    showToastAtCentre("Please Enter Your Email ID", LENGTH_SHORT);
                    return;
                }
                sendPasswordToEmail(email.getText().toString().trim());
                dialog.cancel();
            }
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
    }


    private void sendPasswordToEmail(String email) {

        Utils.showBusyAnimation(baseActivity,"Authenticating..");
        Call<GeneralModel> call = apiService.getForgotPassword("forgot_password",email);

        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {

                Utils.hideBusyAnimation(baseActivity);
                if(response.body() != null) {
                    if (response.body().getResponse().get(0).getStatus().equalsIgnoreCase("success"))
                    {
                        showToastAtCentre("Password sent to your registered Email ID!",LENGTH_LONG);
                    }
                    else // Handle failure cases here
                    {
                        String mess = response.body().getResponse().get(0).getMessage();
                        Log.d(TAG, "Response: " + mess);
                        showToastAtCentre("" + mess, LENGTH_LONG);
                    }
                }

            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                Utils.hideBusyAnimation(baseActivity);
                Log.d(TAG, "Response: " + t.getMessage());
                if (t instanceof IOException) {
                    Toast.makeText(baseActivity, "No Internet Connection!!", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void showToastAtCentre(String message, int duration) {
        if (mToast != null) {
            mToast.cancel();
            mToast = null;
        }
        mToast = Toast.makeText(getApplicationContext(), message, duration);
        mToast.show();
    }
}
