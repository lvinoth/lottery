package com.result.bhutanking.Adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.result.bhutanking.DashboardActivity;
import com.result.bhutanking.Model.HistoryModel;
import com.result.bhutanking.R;

import java.util.List;


public class TransactionHistoryAdapter extends RecyclerView.Adapter<TransactionHistoryAdapter.ViewHolder>  {

    private List<HistoryModel.Transactions> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context mContext;
    private Activity baseActivity;


    // data is passed into the constructor
    public TransactionHistoryAdapter(Context context, List<HistoryModel.Transactions> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.mContext = context;
        this.baseActivity = (DashboardActivity)context;
    }

    // inflates the row layout from xml when needed
    @NonNull
    @Override
    public TransactionHistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.purchase_history_list_item, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final HistoryModel.Transactions transactions = mData.get(position);
        if (transactions.getTransaction_datetime() != null && !TextUtils.isEmpty(transactions.getTransaction_datetime().trim())) {
            holder.date.setText(transactions.getTransaction_datetime().split(" ")[0]);
        }
        else {
            holder.date.setText("");
        }
        holder.subject.setText(transactions.getDescription());
        holder.creditOrDebit.setText(transactions.getType());
        holder.amount.setText("₹ " + String.valueOf(transactions.getAmount()));
        if (position % 2 != 0)
        {
            holder.linearLayout.setBackgroundColor(Color.parseColor("#DDDDDD"));
        }else {
            holder.linearLayout.setBackgroundColor(Color.parseColor("#ECECEC"));
        }

    }


    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView date;
        TextView subject;
        TextView creditOrDebit;
        TextView amount;
        LinearLayout linearLayout;

        ViewHolder(View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.purchaseDate);
            subject = itemView.findViewById(R.id.purchaseSubject);
            creditOrDebit = itemView.findViewById(R.id.purchaseType);
            amount = itemView.findViewById(R.id.purchaseAmount);
            linearLayout = itemView.findViewById(R.id.linearLayout);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }


    // convenience method for getting data at click position
    HistoryModel.Transactions getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
