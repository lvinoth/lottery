package com.result.bhutanking;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.result.bhutanking.Model.TicketDetailsModel;
import com.result.bhutanking.Model.WeeklyTicketPaymentLinkModel;
import com.result.bhutanking.Retrofit.ApiClient;
import com.result.bhutanking.Retrofit.ApiInterface;
import com.result.bhutanking.Utils.SharedPreferenceUtility;
import com.result.bhutanking.Utils.UpdateBalanceInterface;
import com.result.bhutanking.Utils.Utils;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.widget.Toast.LENGTH_LONG;

public class DashboardActivity extends AppCompatActivity implements MyListener, UpdateBalanceInterface {

    private static final String TAG = DashboardActivity.class.getSimpleName();
    private TextView walletBalance;
    private TextView userID;
    private LinearLayout addCashLayout;
    private Button homeButton;
    private LinearLayout myHistoryBtn;
    private Button resultsBtn;
    private LinearLayout my_tickets_btn;
    private Button clientEnquiry;
    private ImageView profileBtn;
    private ScrollView parentScroll;
    FragmentManager fragmentManager;
    private Toast mToast;
    private ApiInterface apiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.makeStatusBarTransparent(getWindow(), this);
        setContentView(R.layout.activity_dashboard);
        int bal = SharedPreferenceUtility.getUserBalance(this);
        String id = SharedPreferenceUtility.getUserId(this);
        parentScroll = findViewById(R.id.parentScroll);
        fragmentManager = getSupportFragmentManager();
        apiService = ApiClient.getClient().create(ApiInterface .class);
        clientEnquiry = findViewById(R.id.clientEnquiry);
        clientEnquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFragment(new InquiryFragment(), InquiryFragment.class.getSimpleName());
            }
        });
        my_tickets_btn = findViewById(R.id.my_tickets_btn);
        my_tickets_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFragment(new TodayHistoryFragment(), TodayHistoryFragment.class.getSimpleName());
            }
        });
        resultsBtn = findViewById(R.id.resultsBtn);
        resultsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFragment(new ResultFragment(), ResultFragment.class.getSimpleName());
            }
        });
        myHistoryBtn = findViewById(R.id.myHistoryBtn);
        myHistoryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFragment(new HistoryFragment(), HistoryFragment.class.getSimpleName());
            }
        });
        profileBtn = findViewById(R.id.profileBtn);
        profileBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFragment(new ProfileFragment(), ProfileFragment.class.getSimpleName());
            }
        });
        homeButton = findViewById(R.id.ticketBooking);
        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFragment(new WelcomeFragment(), WelcomeFragment.class.getSimpleName());
            }
        });
        addCashLayout = findViewById(R.id.addCashLayout);
        addCashLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFragment(new AddCashFragment(), AddCashFragment.class.getSimpleName());
            }
        });
        walletBalance = findViewById(R.id.walletBalance);
        userID = findViewById(R.id.userId);
        walletBalance.setText(" ₹ " + bal);
        userID.setText(id);
        String from = getIntent().getStringExtra("source");
        if (from != null && from.equalsIgnoreCase("Register")) {
            loadFragment(new ProfileFragment(), ProfileFragment.class.getSimpleName());
        } else {
            loadFragment(new WelcomeFragment(), WelcomeFragment.class.getSimpleName());
        }
        getTicketPaymentLinks();
    }

    List<List<WeeklyTicketPaymentLinkModel.Link>> weekly_draw_links;
    private void getTicketPaymentLinks()
    {
        Utils.showBusyAnimation(DashboardActivity.this,"Authenticating..");
        String token = SharedPreferenceUtility.getUserToken(DashboardActivity.this);

        Call<WeeklyTicketPaymentLinkModel> call = apiService.getWeeklyTicketPaymentLinks(token,"weekly_draw_links");

        call.enqueue(new Callback<WeeklyTicketPaymentLinkModel>() {
            @Override
            public void onResponse(Call<WeeklyTicketPaymentLinkModel> call, Response<WeeklyTicketPaymentLinkModel> response) {

                Utils.hideBusyAnimation(DashboardActivity.this);
                if(response.body() != null) {
                    if (response.body().getResponse().get(0).getStatus().equalsIgnoreCase("success"))
                    {
                        weekly_draw_links = response.body().getResponse().get(0).getWeekly_draw();
                        getTicketDetails();
                    }
                    else // Handle failure cases here
                    {
                        String mess = response.body().getResponse().get(0).getMessage();
                        Log.d(TAG, "Response: " + mess);
                        showToastAtCentre("" + mess, LENGTH_LONG);
                    }
                }

            }

            @Override
            public void onFailure(Call<WeeklyTicketPaymentLinkModel> call, Throwable t) {
                Utils.hideBusyAnimation(DashboardActivity.this);
                Log.d(TAG, "Response: " + t.getMessage());
                if (t instanceof IOException) {
                    Toast.makeText(DashboardActivity.this, "No Internet Connection!!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public String getPaymentLink(String price)
    {
        String link = "";
        if (weekly_draw_links != null && weekly_draw_links.size() > 0)
        {
            for (int i =0; i < weekly_draw_links.size();i++)
            {
                if (weekly_draw_links.get(i).get(0).getAmount().equalsIgnoreCase(price)){
                    link = weekly_draw_links.get(i).get(0).getLink();
                    break;
                }
            }
        }
        return link;
    }
    private List<List<TicketDetailsModel.Price>> weeklyPriceList;
    private void getTicketDetails()
    {
        Utils.showBusyAnimation(DashboardActivity.this,"Authenticating..");
        String token = SharedPreferenceUtility.getUserToken(DashboardActivity.this);

        Call<TicketDetailsModel> call = apiService.getTicketDetails(token,"ticket_price");

        call.enqueue(new Callback<TicketDetailsModel>() {
            @Override
            public void onResponse(Call<TicketDetailsModel> call, Response<TicketDetailsModel> response) {

                Utils.hideBusyAnimation(DashboardActivity.this);
                if(response.body() != null) {
                    if (response.body().getResponse().get(0).getStatus().equalsIgnoreCase("success"))
                    {
                        weeklyPriceList =  response.body().getResponse().get(0).getTicket_price().get(0).getWEEKLY();

                    }
                    else // Handle failure cases here
                    {
                        String mess = response.body().getResponse().get(0).getMessage();
                        Log.d(TAG, "Response: " + mess);
                        showToastAtCentre("" + mess, LENGTH_LONG);
                    }
                }

            }

            @Override
            public void onFailure(Call<TicketDetailsModel> call, Throwable t) {
                Utils.hideBusyAnimation(DashboardActivity.this);
                Log.d(TAG, "Response: " + t.getMessage());
                if (t instanceof IOException) {
                    Toast.makeText(DashboardActivity.this, "No Internet Connection!!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public String getWeeklyTicketCost()
    {
        String cost = "0";
        if (weeklyPriceList != null && weeklyPriceList.size() > 0) {
            cost = weeklyPriceList.get(0).get(0).getTicket_price();
        }
        return cost;
    }


    private void showToastAtCentre(String message, int duration) {
        if (mToast != null) {
            mToast.cancel();
            mToast = null;
        }
        mToast = Toast.makeText(getApplicationContext(), message, duration);
        mToast.show();
    }

    private void loadFragment(Fragment fragment, String fragmentName) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, fragment, fragmentName);
        fragmentTransaction.commit();
        parentScroll.smoothScrollTo(0, 0);
        parentScroll.scrollTo(0, 0);
    }

    public void updateBalance(int bal) {
        walletBalance.setText(String.valueOf(" ₹ " + bal));
    }


    @Override
    public void replaceFragments(String fragmentName, String type) {
        if (fragmentName.contains("buy")) {
            loadFragment(new BuyTicketBKFragment(type), BuyTicketBKFragment.class.getSimpleName());
        }
        if (fragmentName.contains("weekly")) {
            loadFragment(new WeeklyPriceFragment(), WeeklyPriceFragment.class.getSimpleName());
        }
        if (fragmentName.contains("add_cash")) {
            loadFragment(new AddCashFragment(), AddCashFragment.class.getSimpleName());
        }
        if (fragmentName.contains("home")) {
            loadFragment(new WelcomeFragment(), WelcomeFragment.class.getSimpleName());
        }
        if (fragmentName.contains("weekly_draw")) {
            loadFragment(new WeeklyBuyFragment(), WeeklyBuyFragment.class.getSimpleName());
        }

    }

    @Override
    public void updateUserBalance(int balance) {
        walletBalance.setText(String.valueOf(" ₹ " + balance));
    }

    private void confirmLogout() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_alert);

        WindowManager.LayoutParams wlp = dialog.getWindow().getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_FULLSCREEN;
        dialog.getWindow().setAttributes(wlp);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
        TextView desc = dialog.findViewById(R.id.alertText);
        TextView cancelBtn = dialog.findViewById(R.id.alertCancel);
        TextView okBtn = dialog.findViewById(R.id.alertOk);

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Invoke Delete API here
                finish();
                dialog.cancel();
            }
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
    }

    @Override
    public void onBackPressed() {
        WelcomeFragment welcomeFragment = (WelcomeFragment) fragmentManager.findFragmentByTag(WelcomeFragment.class.getSimpleName());
        if (welcomeFragment != null && welcomeFragment.isVisible()) {
            // add your code here
            confirmLogout();
        } else {
            loadFragment(new WelcomeFragment(), WelcomeFragment.class.getSimpleName());
        }
    }
}
