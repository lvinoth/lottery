package com.result.bhutanking;

import com.result.bhutanking.Model.TicketSelectionModel;

public interface WeeklyTicketSelectionListener {
    void ticketSelectedOrRemoved(TicketSelectionModel ticketNo, boolean isAddedOrRemoved, boolean showAlert);
}
