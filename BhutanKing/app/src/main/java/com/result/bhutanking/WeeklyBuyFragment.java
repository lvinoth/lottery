package com.result.bhutanking;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.CountDownTimer;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.result.bhutanking.Adapter.WeeklyBuyTicketAdapter;
import com.result.bhutanking.Model.GeneralModel;
import com.result.bhutanking.Model.TicketSelectionModel;
import com.result.bhutanking.Model.WeeklyTicketBuyingDetailsModel;
import com.result.bhutanking.Retrofit.ApiClient;
import com.result.bhutanking.Retrofit.ApiInterface;
import com.result.bhutanking.Utils.GridItemDecorator;
import com.result.bhutanking.Utils.SharedPreferenceUtility;
import com.result.bhutanking.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.widget.Toast.LENGTH_LONG;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link WeeklyBuyFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class WeeklyBuyFragment extends Fragment implements WeeklyTicketSelectionListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = WeeklyBuyFragment.class.getSimpleName();
    private Button payNow;
    private String totalAmountToPay;
    private String starting_number = "";
    private String ending_number = "";
    private int ticket_count;
    private WeeklyBuyTicketAdapter weeklyBuyTicketAdapter;
    private List<TicketSelectionModel> ticketSelectionList;
    private List<TicketSelectionModel> ticketsList;
    private List<WeeklyTicketBuyingDetailsModel.DrawStatus> alreadyBookedTicketList;
    private EditText ticketStartText;
    private EditText ticketEndText;
    private Button generateTickets;
    private String[] zerosToAppend = new String[]{"","0","00","000","0000","00000"};
    private TextView selectedTicketCountTxt;
    private TextView selectedTicketPriceTxt;
    private TextView ticketCost;
    private TextView weeklyTimerTxt;
    private TextView totalAmountToPayTxt;
    private String weeklyPerTicketCost;
    private Activity baseActivity;
    private Toast mToast;
    private ApiInterface apiService;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private RecyclerView ticketRecyclerView;
    private TextView ticketRange;

    public WeeklyBuyFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment WeeklyBuyFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static WeeklyBuyFragment newInstance(String param1, String param2) {
        WeeklyBuyFragment fragment = new WeeklyBuyFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root =  inflater.inflate(R.layout.fragment_weekly_buy, container, false);
        totalAmountToPay = "";
        ticketsList = new ArrayList<>();
        ticketSelectionList = new ArrayList<>();
        ticketRecyclerView = root.findViewById(R.id.ticketRecyclerView);
        ticketRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        ticketRecyclerView.addItemDecoration(new GridItemDecorator(3, 15, true));
        ticketStartText = root.findViewById(R.id.ticketStartNo);
        ticketEndText = root.findViewById(R.id.ticketEndNo);
        selectedTicketCountTxt = root.findViewById(R.id.selectedTicketCount);
        selectedTicketPriceTxt = root.findViewById(R.id.selectedTicketPrice);
        ticketCost = root.findViewById(R.id.ticketCost);
        weeklyTimerTxt = root.findViewById(R.id.weekly_timer);
        totalAmountToPayTxt = root.findViewById(R.id.totalAmountToPay);
        generateTickets = root.findViewById(R.id.generateTickets);
        ticketRange = root.findViewById(R.id.ticketRange);
        weeklyPerTicketCost = ((DashboardActivity)getActivity()).getWeeklyTicketCost();
        generateTickets.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String startNo = ticketStartText.getText().toString().trim();
                String endNo = ticketEndText.getText().toString().trim();
                if (TextUtils.isEmpty(startNo) || TextUtils.isEmpty(endNo))
                {
                    showToastAtCentre("Please enter the ticket range.", LENGTH_LONG);
                    return;
                }
                if (startNo.length() < starting_number.substring(2).length() || endNo.length() < ending_number.substring(2).length())
                {
                    showToastAtCentre("Please enter a valid ticket number between " + starting_number + " and " + ending_number, LENGTH_LONG);
                    return;
                }
                int ticket_start = Integer.parseInt(startNo);
                int ticket_end = Integer.parseInt(endNo);
                int diff = ticket_end - ticket_start;
                if (diff > ticket_count || diff <= 0)
                {
                    showToastAtCentre("Enter the ticket number between " + starting_number + " and " + ending_number, LENGTH_LONG);
                    return;
                }
                //TODO: validations to go here
                ticketsList.clear();
                TicketSelectionModel ticketSelection;
                for (int s = ticket_start; s <= ticket_end; s++)
                {
                    ticketSelection = new TicketSelectionModel();
                    ticketSelection.setSelected(false);
                    int len = startNo.length() - String.valueOf(s).length();
                    String tn = zerosToAppend[len];
                    ticketSelection.setTicketNo("BK" + tn + s);
                    ticketsList.add(ticketSelection);
                }
                weeklyBuyTicketAdapter = new WeeklyBuyTicketAdapter(getActivity(),ticketsList, alreadyBookedTicketList,WeeklyBuyFragment.this);
                ticketRecyclerView.setAdapter(weeklyBuyTicketAdapter);
            }
        });
        payNow = root.findViewById(R.id.weekly_pay);
        payNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ticketSelectionList.size() == 0)
                {
                    showToastAtCentre("Please select minimum of one ticket to continue.",LENGTH_LONG);
                    return;
                }
               buyTickets();

            }
        });
        baseActivity = getActivity();
        apiService = ApiClient.getClient().create(ApiInterface.class);
        selectedTicketCountTxt.setText("0");
        selectedTicketPriceTxt.setText("0 X ₹ " + weeklyPerTicketCost);
        ticketCost.setText(weeklyPerTicketCost+"/");
        totalAmountToPayTxt.setText("₹ 0");
        getTicketDetails();
        return root;
    }

    private JSONObject createTicketJson() {
        JSONObject finalObject = new JSONObject();
        try {
            finalObject.put("draw_no",1);
            JSONArray resultSet = new JSONArray();
            for (int i = 0; i < this.ticketSelectionList.size(); i++) {
                resultSet.put(this.ticketSelectionList.get(i).getTicketNo());
            }

            finalObject.put("ticket_nos", resultSet);
        } catch (Exception ex) {

        }
        return finalObject;
    }

    private void buyTickets() {
        Utils.showBusyAnimation(baseActivity, "Authenticating..");
        String token = SharedPreferenceUtility.getUserToken(getActivity());

        String ticketDetails = createTicketJson().toString();

        Call<GeneralModel> call = apiService.buyTickets(token, "weekly_draw_ticket_purchase", ticketDetails);

        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {

                Utils.hideBusyAnimation(baseActivity);
                if (response.body() != null) {
                    if (response.body().getResponse().get(0).getStatus().equalsIgnoreCase("success")) {
                        Log.d(TAG,"Tickets booked successfully!!");
                        ticketSelectionList.clear();
                        ticketsList.clear();
                        weeklyBuyTicketAdapter.setDataSet(ticketsList);
                        selectedTicketCountTxt.setText("0");
                        selectedTicketPriceTxt.setText("0 X ₹ " + weeklyPerTicketCost);
                        totalAmountToPayTxt.setText("₹ 0");
                        ticketStartText.setText("");
                        ticketEndText.setText("");
                        String link =  ((DashboardActivity)getActivity()).getPaymentLink(totalAmountToPay);
                        Intent i = new Intent(getActivity(), PaymentActivity.class);
                        i.putExtra("amount",link);
                        startActivity(i);

                    } else // Handle failure cases here
                    {
                        String mess = response.body().getResponse().get(0).getMessage();
                        Log.d(TAG, "Response: " + mess);
                        showToastAtCentre("" + mess, LENGTH_LONG);
                    }
                }

            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                Utils.hideBusyAnimation(baseActivity);
                Log.d(TAG, "Response: " + t.getMessage());
                if (t instanceof IOException) {
                    Toast.makeText(baseActivity, "No Internet Connection!!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void showToastAtCentre(String message, int duration) {
        if (mToast != null) {
            mToast.cancel();
            mToast = null;
        }
        mToast = Toast.makeText(getActivity().getApplicationContext(), message, duration);
        mToast.show();
    }

    @Override
    public void ticketSelectedOrRemoved(TicketSelectionModel ticketNo, boolean isAddedOrRemoved, boolean showAlert) {
        if (showAlert)
        {
            showToastAtCentre("Maximum of 10 tickets can be selected", LENGTH_LONG);
            return;
        }
        if (isAddedOrRemoved)
        {
            ticketSelectionList.add(ticketNo);
        }
        else {
            ticketSelectionList.remove(ticketNo);
        }
        selectedTicketCountTxt.setText(ticketSelectionList.size()+"");
        selectedTicketPriceTxt.setText(ticketSelectionList.size() + " X ₹ " + weeklyPerTicketCost);
        int totalAmount = ticketSelectionList.size() * Integer.parseInt(weeklyPerTicketCost);
        totalAmountToPay = String.valueOf(totalAmount);
        totalAmountToPayTxt.setText("₹ " + totalAmountToPay);
    }

    private void getTicketDetails()
    {
        Utils.showBusyAnimation(baseActivity,"Authenticating..");
        String token = SharedPreferenceUtility.getUserToken(baseActivity);

        Call<WeeklyTicketBuyingDetailsModel> call = apiService.getWeeklyTicketDrawDetails(token,"weekly_draw_details");

        call.enqueue(new Callback<WeeklyTicketBuyingDetailsModel>() {
            @Override
            public void onResponse(Call<WeeklyTicketBuyingDetailsModel> call, Response<WeeklyTicketBuyingDetailsModel> response) {

                Utils.hideBusyAnimation(baseActivity);
                if(response.body() != null) {
                    if (response.body().getResponse().get(0).getStatus().equalsIgnoreCase("success"))
                    {
                        alreadyBookedTicketList = response.body().getResponse().get(0).getDraw_ticket_status();
                        ticket_count = response.body().getResponse().get(0).getDraw_details().get(0).getTicket_count();
                        starting_number = response.body().getResponse().get(0).getDraw_details().get(0).getStarting_number();
                        String tNo = starting_number.substring(2);
                        ticketStartText.setFilters(new InputFilter[] { new InputFilter.LengthFilter(tNo.length()) });
                        ticketEndText.setFilters(new InputFilter[] { new InputFilter.LengthFilter(tNo.length()) });
                        int ticketEndNo = Integer.parseInt(tNo) + ticket_count - 1;
                        int diff = tNo.length() - String.valueOf(ticketEndNo).length();
                        String appendZero = "";
                        for (int d = 0; d < diff; d++){
                            appendZero+="0";
                        }
                        ticketStartText.setHint(tNo);
                        ticketEndText.setHint(appendZero + ticketEndNo);
                        ending_number = "BK"+ appendZero + ticketEndNo;
                        ticketRange.setText(starting_number + " to " + ending_number);

                        String weekly_time = response.body().getResponse().get(0).getDraw_details().get(0).getDraw_to_date() + " " + response.body().getResponse().get(0).getDraw_details().get(0).getDraw_to_date_time();
                        if (TextUtils.isEmpty(weekly_time.trim()))
                        {
                            payNow.setEnabled(false);
                            weeklyTimerTxt.setText("Closed");
                        }
                        else {
                            payNow.setEnabled(true);
                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            try {
                                String time1 = dateFormat.format(new Date());

                                Date date = dateFormat.parse(weekly_time);
                                Date date1 = dateFormat.parse(time1);
                                long difference =  date.getTime() - date1.getTime();

                                long Minutes = difference / (60 * 1000) % 60;
                                long minutesInMillis = TimeUnit.MINUTES.toMillis(Minutes);
                                long secs = difference / 1000;
                                long secsInMillis = TimeUnit.SECONDS.toMillis(secs % 60);

                                // close the timer 10 mins before
                                long HoursToMins = (difference / ( 60 * 60 * 1000) % 24) * 60;
                                long oneMinuteInMilliSecs = 60000;
                                long millies = (difference - (oneMinuteInMilliSecs * 90) );

                                if (millies < 0)
                                {
                                    //weeklyIntervalCounter(minutesInMillis + secsInMillis);
                                    ((DashboardActivity)getActivity()).onBackPressed();
                                }
                                else {
                                    weeklyCountdownTimer(millies);
                                }

                            } catch (ParseException e) {
                            }
                        }

                    }
                    else // Handle failure cases here
                    {
                        String mess = response.body().getResponse().get(0).getMessage();
                        Log.d(TAG, "Response: " + mess);
                        showToastAtCentre("" + mess, LENGTH_LONG);
                    }
                }

            }

            @Override
            public void onFailure(Call<WeeklyTicketBuyingDetailsModel> call, Throwable t) {
                Utils.hideBusyAnimation(baseActivity);
                Log.d(TAG, "Response: " + t.getMessage());
                if (t instanceof IOException) {
                    Toast.makeText(baseActivity, "No Internet Connection!!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private CountDownTimer weekly_countDownTimer;
    private void weeklyCountdownTimer(long timer)
    {
        if (weekly_countDownTimer != null)
        {
            weekly_countDownTimer.cancel();
            weekly_countDownTimer = null;
        }
        weekly_countDownTimer = new CountDownTimer(timer, 1000) {

            public void onTick(long millisUntilFinished) {
                //here you can have your logic to set text to edittext
                long millis = millisUntilFinished;
                @SuppressLint("DefaultLocale") String hms = String.format("%02d Days : %02d Hours : %02d Min : %02d Secs", TimeUnit.MILLISECONDS.toDays(millis),TimeUnit.MILLISECONDS.toHours(millis),
                        TimeUnit.MILLISECONDS.toMinutes(millis) % TimeUnit.HOURS.toMinutes(1),
                        TimeUnit.MILLISECONDS.toSeconds(millis) % TimeUnit.MINUTES.toSeconds(1));
                weeklyTimerTxt.setText(hms);
            }

            public void onFinish() {
//                payNow.setEnabled(false);
//                weeklyTimerTxt.setText("Waiting");
//                weeklyIntervalCounter(5400000);
                ((DashboardActivity)getActivity()).onBackPressed();
            }

        }.start();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (weekly_countDownTimer != null)
        {
            weekly_countDownTimer.cancel();
            weekly_countDownTimer = null;
        }
    }
}
