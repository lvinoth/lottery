package com.result.bhutanking.Model;

import java.util.List;

/**
 * Created by shastamobiledev on 03/04/18.
 */

public class ProfileModel {

    private List<Response> response;

    public List<Response> getResponse() {
        return response;
    }

    public void setResponse(List<Response> response) {
        this.response = response;
    }

    public class Response
    {

        private User user;
        private Bank bank;
        private String status;
        private String message;

        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user = user;
        }

        public Bank getBank() {
            return bank;
        }

        public void setBank(Bank bank) {
            this.bank = bank;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public class User{
        private String name;
        private String email;
        private String mobile;
        private String reg_id;
        private int available_balance;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getReg_id() {
            return reg_id;
        }

        public void setReg_id(String reg_id) {
            this.reg_id = reg_id;
        }

        public int getAvailable_balance() {
            return available_balance;
        }

        public void setAvailable_balance(int available_balance) {
            this.available_balance = available_balance;
        }
    }

    public class Bank {
        private String bank_name;
        private String acc_holder_name;
        private String acc_number;
        private String acc_type;
        private String ifsc_code;
        private String upi_code;

        public String getBank_name() {
            return bank_name;
        }

        public void setBank_name(String bank_name) {
            this.bank_name = bank_name;
        }

        public String getAcc_holder_name() {
            return acc_holder_name;
        }

        public void setAcc_holder_name(String acc_holder_name) {
            this.acc_holder_name = acc_holder_name;
        }

        public String getAcc_number() {
            return acc_number;
        }

        public void setAcc_number(String acc_number) {
            this.acc_number = acc_number;
        }

        public String getAcc_type() {
            return acc_type;
        }

        public void setAcc_type(String acc_type) {
            this.acc_type = acc_type;
        }

        public String getIfsc_code() {
            return ifsc_code;
        }

        public void setIfsc_code(String ifsc_code) {
            this.ifsc_code = ifsc_code;
        }

        public String getUpi_code() {
            return upi_code;
        }

        public void setUpi_code(String upi_code) {
            this.upi_code = upi_code;
        }
    }

}
