package com.result.bhutanking.Model;

import java.util.List;

public class AgentBankDetailsModel {

    private List<Details> response;

    public List<Details> getResponse() {
        return response;
    }

    public void setResponse(List<Details> response) {
        this.response = response;
    }

    public class Details {

        private String agent;
        private String bank_name;
        private String account_name;
        private String account_number;
        private String ifsc_code;
        private String upi_code;
        private String qr_code;
        private String status;
        private String message;

        public String getAgent() {
            return agent;
        }

        public void setAgent(String agent) {
            this.agent = agent;
        }

        public String getBank_name() {
            return bank_name;
        }

        public void setBank_name(String bank_name) {
            this.bank_name = bank_name;
        }

        public String getAccount_name() {
            return account_name;
        }

        public void setAccount_name(String account_name) {
            this.account_name = account_name;
        }

        public String getAccount_number() {
            return account_number;
        }

        public void setAccount_number(String account_number) {
            this.account_number = account_number;
        }

        public String getIfsc_code() {
            return ifsc_code;
        }

        public void setIfsc_code(String ifsc_code) {
            this.ifsc_code = ifsc_code;
        }

        public String getUpi_code() {
            return upi_code;
        }

        public void setUpi_code(String upi_code) {
            this.upi_code = upi_code;
        }

        public String getQr_code() {
            return qr_code;
        }

        public void setQr_code(String qr_code) {
            this.qr_code = qr_code;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
