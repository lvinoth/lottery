package com.result.bhutanking.Model;

import java.util.List;

public class WeeklyTicketBuyingDetailsModel {

    private List<Response> response;

    public List<Response> getResponse() {
        return response;
    }

    public void setResponse(List<Response> response) {
        this.response = response;
    }

    public class Response
    {
        private String status;
        private String message;
        private List<DrawDetails> draw_details;
        private List<DrawStatus> draw_ticket_status;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }


        public List<DrawDetails> getDraw_details() {
            return draw_details;
        }

        public void setDraw_details(List<DrawDetails> draw_details) {
            this.draw_details = draw_details;
        }

        public List<DrawStatus> getDraw_ticket_status() {
            return draw_ticket_status;
        }

        public void setDraw_ticket_status(List<DrawStatus> draw_ticket_status) {
            this.draw_ticket_status = draw_ticket_status;
        }
    }

    public class DrawDetails{
        private int draw_no;
        private String draw_from_date;
        private String draw_to_date;
        private String draw_to_date_time;
        private int ticket_count;
        private String starting_number;

        public int getDraw_no() {
            return draw_no;
        }

        public void setDraw_no(int draw_no) {
            this.draw_no = draw_no;
        }

        public String getDraw_from_date() {
            return draw_from_date;
        }

        public void setDraw_from_date(String draw_from_date) {
            this.draw_from_date = draw_from_date;
        }

        public String getDraw_to_date() {
            return draw_to_date;
        }

        public void setDraw_to_date(String draw_to_date) {
            this.draw_to_date = draw_to_date;
        }

        public int getTicket_count() {
            return ticket_count;
        }

        public void setTicket_count(int ticket_count) {
            this.ticket_count = ticket_count;
        }

        public String getStarting_number() {
            return starting_number;
        }

        public void setStarting_number(String starting_number) {
            this.starting_number = starting_number;
        }

        public String getDraw_to_date_time() {
            return draw_to_date_time;
        }

        public void setDraw_to_date_time(String draw_to_date_time) {
            this.draw_to_date_time = draw_to_date_time;
        }
    }

    public class DrawStatus {
        private String ticket_no;

        public String getTicket_no() {
            return ticket_no;
        }

        public void setTicket_no(String ticket_no) {
            this.ticket_no = ticket_no;
        }
    }
}
