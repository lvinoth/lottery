package com.result.bhutanking.Model;

public class TicketModel {

    private String ticket_no;
    private String ticket_no_position;
    private String ticket_count;
    private String ticket_price;

    public String getTicket_no() {
        return ticket_no;
    }

    public void setTicket_no(String ticket_no) {
        this.ticket_no = ticket_no;
    }

    public String getTicket_no_position() {
        return ticket_no_position;
    }

    public void setTicket_no_position(String ticket_no_position) {
        this.ticket_no_position = ticket_no_position;
    }

    public String getTicket_count() {
        return ticket_count;
    }

    public void setTicket_count(String ticket_count) {
        this.ticket_count = ticket_count;
    }

    public String getTicket_price() {
        return ticket_price;
    }

    public void setTicket_price(String ticket_price) {
        this.ticket_price = ticket_price;
    }
}
