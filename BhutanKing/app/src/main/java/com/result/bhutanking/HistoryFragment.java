package com.result.bhutanking;

import android.app.Activity;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.result.bhutanking.Adapter.TicketPurchaseHistoryAdapter;
import com.result.bhutanking.Adapter.TransactionHistoryAdapter;
import com.result.bhutanking.Model.HistoryModel;
import com.result.bhutanking.Retrofit.ApiClient;
import com.result.bhutanking.Retrofit.ApiInterface;
import com.result.bhutanking.Utils.DateComparator;
import com.result.bhutanking.Utils.DateComparatorPayment;
import com.result.bhutanking.Utils.SharedPreferenceUtility;
import com.result.bhutanking.Utils.Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.widget.Toast.LENGTH_LONG;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HistoryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HistoryFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = HistoryFragment.class.getSimpleName();
    private Activity baseActivity;
    private Toast mToast;
    private ApiInterface apiService;
    private RecyclerView transactionRecyclerView;
    private RecyclerView ticketHistoryRecyclerView;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public HistoryFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HistoryFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HistoryFragment newInstance(String param1, String param2) {
        HistoryFragment fragment = new HistoryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        baseActivity = getActivity();
        apiService = ApiClient.getClient().create(ApiInterface .class);
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_history, container, false);
        transactionRecyclerView = root.findViewById(R.id.paymentRecyclerView);
        ticketHistoryRecyclerView = root.findViewById(R.id.ticketRecyclerView);
        getTicketHistory();
        return root;
    }

    List<HistoryModel.Transactions> historyTransactionsList = new ArrayList<>();
    List<HistoryModel.Ticket_Purchase_History> historyTicketPurchaseList = new ArrayList<>();
    TransactionHistoryAdapter transactionHistoryAdapter;// = new TransactionHistoryAdapter(getActivity(), historyTransactionsList);
    TicketPurchaseHistoryAdapter ticketPurchaseHistoryAdapter;// = new TicketPurchaseHistoryAdapter(getActivity(),historyTicketPurchaseList);

    private void getTicketHistory()
    {
        Utils.showBusyAnimation(baseActivity,"Authenticating..");
        String token = SharedPreferenceUtility.getUserToken(getActivity());

        Call<HistoryModel> call = apiService.getHistory(token,"trans_ticket_purchase_history");

        call.enqueue(new Callback<HistoryModel>() {
            @Override
            public void onResponse(Call<HistoryModel> call, Response<HistoryModel> response) {

                Utils.hideBusyAnimation(baseActivity);
                if(response.body() != null) {
                    if (response.body().getResponse().get(0).getStatus().equalsIgnoreCase("success"))
                    {
                        int transactionsCount = response.body().getResponse().get(0).getTransactions().size();
                        HistoryModel.Transactions trans;
                        for (int i = 0; i < transactionsCount; i++){
                            trans = (HistoryModel.Transactions)response.body().getResponse().get(0).getTransactions().get(i).get(0);
                            historyTransactionsList.add(trans);
                        }
                        Collections.sort(historyTransactionsList, new DateComparatorPayment());
                        Collections.reverse(historyTransactionsList);
                        transactionHistoryAdapter = new TransactionHistoryAdapter(getActivity(),historyTransactionsList);
                        transactionRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
                        transactionRecyclerView.setAdapter(transactionHistoryAdapter);

                        int ticketHistoryCount = response.body().getResponse().get(0).getTicket_purchase_history().size();
                        HistoryModel.Ticket_Purchase_History ticketPurchaseHistory;
                        for (int i = 0; i < ticketHistoryCount; i++){
                            ticketPurchaseHistory = (HistoryModel.Ticket_Purchase_History)response.body().getResponse().get(0).getTicket_purchase_history().get(i).get(0);
                            historyTicketPurchaseList.add(ticketPurchaseHistory);
                        }
                        Collections.sort(historyTicketPurchaseList, new DateComparator());
                        Collections.reverse(historyTicketPurchaseList);
                        ticketPurchaseHistoryAdapter = new TicketPurchaseHistoryAdapter(getActivity(),historyTicketPurchaseList);
                        ticketHistoryRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
                        ticketHistoryRecyclerView.setAdapter(ticketPurchaseHistoryAdapter);

                    }
                    else // Handle failure cases here
                    {
                        String mess = response.body().getResponse().get(0).getMessage();
                        Log.d(TAG, "Response: " + mess);
                        showToastAtCentre("" + mess, LENGTH_LONG);
                    }
                }

            }

            @Override
            public void onFailure(Call<HistoryModel> call, Throwable t) {
                Utils.hideBusyAnimation(baseActivity);
                Log.d(TAG, "Response: " + t.getMessage());
                if (t instanceof IOException) {
                    Toast.makeText(baseActivity, "No Internet Connection!!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void showToastAtCentre(String message, int duration) {
        if (mToast != null) {
            mToast.cancel();
            mToast = null;
        }
        mToast = Toast.makeText(getActivity().getApplicationContext(), message, duration);
        mToast.show();
    }
}
