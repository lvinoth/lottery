package com.result.bhutanking;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.os.CountDownTimer;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;
import com.result.bhutanking.Model.ResultModel;
import com.result.bhutanking.Model.WeeklyResultModel;
import com.result.bhutanking.Retrofit.ApiClient;
import com.result.bhutanking.Retrofit.ApiInterface;
import com.result.bhutanking.Utils.DownloadFile;
import com.result.bhutanking.Utils.SharedPreferenceUtility;
import com.result.bhutanking.Utils.Utils;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.widget.Toast.LENGTH_LONG;
import static android.widget.Toast.LENGTH_SHORT;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ResultFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ResultFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = ResultFragment.class.getSimpleName();
    private Activity baseActivity;
    private Toast mToast;
    private ApiInterface apiService;
    private TextView firstPrice;
    private TextView secondPrice;
    private TextView thirdPrice;
    private TextView downloadResult;
    private TextView resultTimerTxt;

    private LinearLayout resultLayout;
    private LinearLayout timerLayout;

    private TextView bkShowCalendar;
    private TextView klShowCalendar;

    private EditText resultTicketNo;
    private Button viewResult;

    private TextView keralaLotteryResultTxt;
    private LinearLayout pdf_download;
    private String chartDownloadPath = "";
    private String firstSlot = "11:00:00";
    private String secondSlot = "13:00:00";
    private String thirdSlot = "18:00:00";
    private String fourthSlot = "19:25:00";
    private String selectedDateString = "";
    private String todayDateString = DateFormat.format("yyyy-MM-dd", new Date()).toString();
    private String[] priceAppend = new String[]{"st","nd","rd","th","th","th","th"};
    private TabLayout calendarTab;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String ticketStartingNumber;

    public ResultFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ResultFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ResultFragment newInstance(String param1, String param2) {
        ResultFragment fragment = new ResultFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_result, container, false);
        baseActivity = getActivity();
        apiService = ApiClient.getClient().create(ApiInterface.class);
        firstPrice = root.findViewById(R.id.firstPrize);
        secondPrice = root.findViewById(R.id.secondPrize);
        thirdPrice = root.findViewById(R.id.thirdPrize);
        pdf_download = root.findViewById(R.id.pdf_download);
        pdf_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkPermissions();
            }
        });
        keralaLotteryResultTxt = root.findViewById(R.id.keralaLotteryResultTxt);
        resultTimerTxt = root.findViewById(R.id.resultTimerTxt);
        resultLayout = root.findViewById(R.id.resultLayout);
        timerLayout = root.findViewById(R.id.timerLayout);
        downloadResult = root.findViewById(R.id.downloadResult);
        resultTicketNo = root.findViewById(R.id.resultTicketNo);
        viewResult = root.findViewById(R.id.viewResult);
        viewResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ticketNo = resultTicketNo.getText().toString().trim();
                if (ticketStartingNumber != null && !TextUtils.isEmpty(ticketStartingNumber)) {
                    int len = ticketStartingNumber.substring(2).length();
                    if (ticketNo.length() != len) {
                        showToastAtCentre("Please enter a valid ticket number", LENGTH_LONG);
                        return;
                    }
                }
                getWeeklyResults("BK"+ticketNo);
            }
        });
//        downloadResult.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showToastAtCentre("Coming soon!!", LENGTH_SHORT);
//            }
//        });
        bkShowCalendar = root.findViewById(R.id.bk_show_calendar);
        klShowCalendar = root.findViewById(R.id.kl_show_calendar);

        bkShowCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker(bkShowCalendar, true);
            }
        });
        klShowCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker(klShowCalendar, false);
            }
        });
        calendarTab = root.findViewById(R.id.tabLayout);
        calendarTab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int pos = tab.getPosition();
                switch (pos)
                {
                    case 0:
                        if (!todayDateString.equalsIgnoreCase(selectedDateString))
                        {
                            getSelectedDateResult(selectedDateString, pos);
                            timerLayout.setVisibility(View.GONE);
                            resultLayout.setVisibility(View.VISIBLE);
                           return;
                        }
                        long slotTimeDiff = getTimeDifference(firstSlot);
                        if (slotTimeDiff > 0)
                        {
                            // result not published
                            startFirstSlotTimer(slotTimeDiff, pos);
                            timerLayout.setVisibility(View.VISIBLE);
                            resultLayout.setVisibility(View.GONE);
                        }else {
                            getSelectedDateResult(selectedDateString, pos);
                            timerLayout.setVisibility(View.GONE);
                            resultLayout.setVisibility(View.VISIBLE);
                        }

                        break;
                    case 1:
                        if (!todayDateString.equalsIgnoreCase(selectedDateString))
                        {
                            getSelectedDateResult(selectedDateString, pos);
                            timerLayout.setVisibility(View.GONE);
                            resultLayout.setVisibility(View.VISIBLE);
                            return;
                        }
                        long slotTimeDiff1 = getTimeDifference(secondSlot);
                        if (slotTimeDiff1 > 0)
                        {
                            // result not published
                            startFirstSlotTimer(slotTimeDiff1, pos);
                            timerLayout.setVisibility(View.VISIBLE);
                            resultLayout.setVisibility(View.GONE);
                        }
                        else {
                            getSelectedDateResult(selectedDateString, pos);
                            timerLayout.setVisibility(View.GONE);
                            resultLayout.setVisibility(View.VISIBLE);
                        }

                        break;
                    case 2:
                        if (!todayDateString.equalsIgnoreCase(selectedDateString))
                        {
                            getSelectedDateResult(selectedDateString, pos);
                            timerLayout.setVisibility(View.GONE);
                            resultLayout.setVisibility(View.VISIBLE);
                            return;
                        }
                        long slotTimeDiff2 = getTimeDifference(thirdSlot);
                        if (slotTimeDiff2 > 0)
                        {
                            // result not published
                            startFirstSlotTimer(slotTimeDiff2, pos);
                            timerLayout.setVisibility(View.VISIBLE);
                            resultLayout.setVisibility(View.GONE);
                        }
                        else {
                            getSelectedDateResult(selectedDateString, pos);
                            timerLayout.setVisibility(View.GONE);
                            resultLayout.setVisibility(View.VISIBLE);
                        }

                        break;
                    case 3:
                        if (!todayDateString.equalsIgnoreCase(selectedDateString))
                        {
                            getSelectedDateResult(selectedDateString, pos);
                            timerLayout.setVisibility(View.GONE);
                            resultLayout.setVisibility(View.VISIBLE);
                            return;
                        }
                        long slotTimeDiff3 = getTimeDifference(fourthSlot);
                        if (slotTimeDiff3 > 0)
                        {
                            // result not published
                            startFirstSlotTimer(slotTimeDiff3, pos);
                            timerLayout.setVisibility(View.VISIBLE);
                            resultLayout.setVisibility(View.GONE);
                        }
                        else {
                            getSelectedDateResult(selectedDateString, pos);
                            timerLayout.setVisibility(View.GONE);
                            resultLayout.setVisibility(View.VISIBLE);
                        }

                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                onTabSelected(tab);
            }
        });
        getResults(0);
//        test();
        return root;
    }

    private void getWeeklyResults(final String ticketNo) {

        Utils.showBusyAnimation(baseActivity,"Authenticating..");
        String token = SharedPreferenceUtility.getUserToken(getActivity());

        Call<WeeklyResultModel> call = apiService.getWeeklyDrawResults(token,"weekly_draw_latest_result");

        call.enqueue(new Callback<WeeklyResultModel>() {
            @Override
            public void onResponse(Call<WeeklyResultModel> call, Response<WeeklyResultModel> response) {

                Utils.hideBusyAnimation(baseActivity);
                if(response.body() != null) {
                    if (response.body().getResponse().get(0).getStatus().equalsIgnoreCase("success"))
                    {
                        HashMap<String, List<String>> winning_nos = response.body().getResponse().get(0).getWinning_nos();
                        boolean isExists = false;
                        String winningPlace = "";
                        for (HashMap.Entry<String, List<String>> entry : winning_nos.entrySet()) {
                            winningPlace = entry.getKey();
                            List<String> value = entry.getValue();
                            List<String> ticketWinningNos = Arrays.asList((value.get(0).split(",")));
                            isExists = ticketWinningNos.contains(ticketNo);
                            resultTicketNo.setText("");
                            if (isExists)
                            {
                                break;
                            }
                        }

                        if (isExists)
                        {
                            showWinningPlace("Congratulations!, You won the " + winningPlace + priceAppend[Integer.parseInt(winningPlace) - 1] +" Price");
                        }
                        else {
                            showWinningPlace("You did not win the price. Better luck next time");
                        }

                    }
                    else
                    {
                        String mess = response.body().getResponse().get(0).getMessage();
                        Log.d(TAG, "Response: " + mess);
                        showToastAtCentre("" + mess, LENGTH_LONG);
                    }
                }

            }

            @Override
            public void onFailure(Call<WeeklyResultModel> call, Throwable t) {
                Utils.hideBusyAnimation(baseActivity);
                Log.d(TAG, "Response: " + t.getMessage());
                if (t instanceof IOException) {
                    Toast.makeText(baseActivity, "No Internet Connection!!", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void showWinningPlace(String winningText) {
        final Dialog dialog = new Dialog(baseActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.winning_alert);

        WindowManager.LayoutParams wlp = dialog.getWindow().getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_FULLSCREEN;
        dialog.getWindow().setAttributes(wlp);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
        TextView desc = dialog.findViewById(R.id.alertText);
        desc.setText(winningText);
        TextView okBtn = dialog.findViewById(R.id.alertOk);

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (firstSlotTimer != null) {
            firstSlotTimer.cancel();
            firstSlotTimer = null;
        }
    }

    private CountDownTimer firstSlotTimer;

    private void startFirstSlotTimer(long slotTimeDiff, final int index) {
        if (firstSlotTimer != null) {
            firstSlotTimer.cancel();
            firstSlotTimer = null;
        }
        firstSlotTimer = new CountDownTimer(slotTimeDiff, 1000) {

            public void onTick(long millisUntilFinished) {
                //here you can have your logic to set text to edittext
                long millis = millisUntilFinished;
                @SuppressLint("DefaultLocale") String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
                        TimeUnit.MILLISECONDS.toMinutes(millis) % TimeUnit.HOURS.toMinutes(1),
                        TimeUnit.MILLISECONDS.toSeconds(millis) % TimeUnit.MINUTES.toSeconds(1));
                resultTimerTxt.setText(hms);
            }

            public void onFinish() {
//                buyTicketBtn.setEnabled(false);
//                lottery_timer.setText("Waiting");
//                intervalCounter(600000);
                getResults(index);
            }

        }.start();
    }

    private long getTimeDifference(String time)
    {
        long difference = 0;
        long millies = 0;
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
            String currentTime = dateFormat.format(new Date());
            Date slotDate = dateFormat.parse(time);
            Date currentDate = dateFormat.parse(currentTime);
            difference = slotDate.getTime() - currentDate.getTime();
            long Minutes = difference / (60 * 1000) % 60;

            // close the timer 10 mins before
            long HoursToMins = (difference / (60 * 60 * 1000) % 24) * 60;
            millies = difference;//(TimeUnit.MINUTES.toMillis(Minutes + HoursToMins) - 600000);
        }
        catch (Exception ex)
        {

        }
        return millies;

    }

    private long test()
    {
        String time = "11:00:00";
        String time1 = "10:58:10";
        long difference = 0;
        long millies = 0;
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

            Date slotDate = dateFormat.parse(time);
            Date currentDate = dateFormat.parse(time1);
            difference = slotDate.getTime() - currentDate.getTime();
            long secs = difference / 1000;
            long Minutes = difference / (60 * 1000) % 60;
            long secsInMillis = TimeUnit.SECONDS.toMillis(secs % 60);

            // close the timer 10 mins before
            long HoursToMins = (difference / (60 * 60 * 1000) % 24) * 60;
            millies = (TimeUnit.MINUTES.toMillis(Minutes + HoursToMins) - 600000);

            Log.e(ResultFragment.class.getSimpleName(),"Seconds: "+ secs);
        }
        catch (Exception ex)
        {

        }
        return millies;

    }



    private HashMap<String, List<List<ResultModel.Result>>> bkResultData;
    private HashMap<String, List<List<ResultModel.Result>>> klResultData;
    private List<List<ResultModel.Result>> selectedResult;
    private void getResults(final int tabIndex)
    {
        Utils.showBusyAnimation(baseActivity,"Authenticating..");
        String token = SharedPreferenceUtility.getUserToken(getActivity());

        Call<ResultModel> call = apiService.getResults(token,"result_bk_kl");

        call.enqueue(new Callback<ResultModel>() {
            @Override
            public void onResponse(Call<ResultModel> call, Response<ResultModel> response) {

                Utils.hideBusyAnimation(baseActivity);
                if(response.body() != null) {
                    if (response.body().getResponse().get(0).getStatus().equalsIgnoreCase("success"))
                    {
                        chartDownloadPath = response.body().getResponse().get(0).getResult_chart();
                        ticketStartingNumber = response.body().getResponse().get(0).getLast_draw_starting_number();
                        resultTicketNo.setFilters(new InputFilter[] { new InputFilter.LengthFilter(ticketStartingNumber.substring(2).length()) });
                        bkResultData = response.body().getResponse().get(0).getButan_king_result();
                        klResultData = response.body().getResponse().get(0).getKerala_result();
                        //DateFormat df = new android.text.format.DateFormat();
                        selectedDateString = DateFormat.format("yyyy-MM-dd", new Date()).toString();
//                        getSelectedDateResult(date);
//                        calendarTab.selectTab(null);
                        calendarTab.selectTab(calendarTab.getTabAt(tabIndex));
                        getKLSelectedDateResult(selectedDateString);

                    }
                    else
                    {
                        String mess = response.body().getResponse().get(0).getMessage();
                        Log.d(TAG, "Response: " + mess);
                        showToastAtCentre("" + mess, LENGTH_LONG);
                    }
                }

            }

            @Override
            public void onFailure(Call<ResultModel> call, Throwable t) {
                Utils.hideBusyAnimation(baseActivity);
                Log.d(TAG, "Response: " + t.getMessage());
                if (t instanceof IOException) {
                    Toast.makeText(baseActivity, "No Internet Connection!!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void getKLSelectedDateResult(String date)
    {
        List<List<ResultModel.Result>> selectedResult1 = klResultData.get(date);
        if (selectedResult1 != null ) {
            if(selectedResult1.size() > 0) {
                keralaLotteryResultTxt.setText(selectedResult1.get(0).get(0).getWinning_no());
            }
            else {
                //showToastAtCentre("No Results found",LENGTH_SHORT);
                keralaLotteryResultTxt.setText("-");
            }
        }
        else {
            showToastAtCentre("Please select a date from last 30 days",LENGTH_SHORT);
            keralaLotteryResultTxt.setText("-");

        }
    }

    private void getSelectedDateResult(String date, int index)
    {
        selectedResult = bkResultData.get(date);
        if (selectedResult != null ) {
            if(selectedResult.size() > 0) {
                firstPrice.setText(selectedResult.get(index).get(0).getWinning_no());
                secondPrice.setText(selectedResult.get(index).get(0).getWinning_no().substring(1));
                thirdPrice.setText(selectedResult.get(index).get(0).getWinning_no().substring(2));
            }
            else {
                //showToastAtCentre("No Results found",LENGTH_SHORT);
                firstPrice.setText("-");
                secondPrice.setText("-");
                thirdPrice.setText("-");
            }
        }
        else {
            showToastAtCentre("Please select a date from last 30 days",LENGTH_SHORT);
            firstPrice.setText("-");
            secondPrice.setText("-");
            thirdPrice.setText("-");
        }
    }

    private void download()
    {
        if (chartDownloadPath != null && !TextUtils.isEmpty(chartDownloadPath))
        {
            new DownloadFile(getActivity()).execute(chartDownloadPath, "bkchart.pdf");
        }
        else {
            showToastAtCentre("Chart not available for download",LENGTH_SHORT);
        }
    }


    private void showToastAtCentre(String message, int duration) {
        if (mToast != null) {
            mToast.cancel();
            mToast = null;
        }
        mToast = Toast.makeText(getActivity().getApplicationContext(), message, duration);
        mToast.show();
    }

    private DatePickerDialog datePickerDialog;
    public void showDatePicker(final TextView date, final boolean isBKLottery)
    {

        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR); // current year
        int mMonth = c.get(Calendar.MONTH); // current month
        int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
        // date picker dialog
        datePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        // set day of month , month and year value in the edit text
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        Calendar isDateSelected = Calendar.getInstance();
                        isDateSelected.set(year, monthOfYear, dayOfMonth);

                        String dateString = sdf.format(isDateSelected.getTime());
                        selectedDateString = dateString;
                        date.setText(dateString);

                        if (isBKLottery) {
                            //getSelectedDateResult(selectedDateString,0);
                            calendarTab.selectTab(calendarTab.getTabAt(0));
                        }
                        else {
                            getKLSelectedDateResult(dateString);
                        }

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH,-1);
        datePickerDialog.getDatePicker().setMinDate(cal.getTimeInMillis());
        datePickerDialog.show();
    }

    private static final int PERMISSION_CODE = 1000;
    private boolean isPermisionGranted = false;
    public void checkPermissions()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED ||
                    getActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                String[] permission = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
                requestPermissions(permission, PERMISSION_CODE);
                isPermisionGranted = false;
            } else {
                // permission granted
                isPermisionGranted = true;
                download();
            }
        } else {
            // os > M
            isPermisionGranted = true;
            download();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    isPermisionGranted = true;
                    download();
                } else {
                    Toast.makeText(getActivity(), "Permission Denied!!", Toast.LENGTH_SHORT).show();
                    isPermisionGranted = false;
                }
        }
    }
}
