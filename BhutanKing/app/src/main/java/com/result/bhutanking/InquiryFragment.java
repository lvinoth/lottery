package com.result.bhutanking;

import android.app.Activity;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.result.bhutanking.Model.GeneralModel;
import com.result.bhutanking.Retrofit.ApiClient;
import com.result.bhutanking.Retrofit.ApiInterface;
import com.result.bhutanking.Utils.SharedPreferenceUtility;
import com.result.bhutanking.Utils.Utils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.widget.Toast.LENGTH_LONG;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link InquiryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class InquiryFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = InquiryFragment.class.getSimpleName();
    private Toast mToast;
    private Activity baseActivity;
    private ApiInterface apiService;
    private EditText enquiryNameTxt;
    private EditText enquiryMobileTxt;
    private EditText enquiryMailTxt;
    private EditText enquiryTextTxt;
    private Button submitEnquiryBtn;
    private Spinner userType;
    private String selectedUserType;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public InquiryFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment InquiryFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static InquiryFragment newInstance(String param1, String param2) {
        InquiryFragment fragment = new InquiryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root =  inflater.inflate(R.layout.fragment_inquiry, container, false);

        baseActivity = getActivity();
        apiService = ApiClient.getClient().create(ApiInterface.class);
        enquiryNameTxt = root.findViewById(R.id.enquiryNameTxt);
        enquiryMobileTxt = root.findViewById(R.id.enquiryMobileTxt);
        enquiryMailTxt = root.findViewById(R.id.enquiryMailTxt);
        enquiryTextTxt = root.findViewById(R.id.enquiryTextTxt);
        enquiryTextTxt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    submitEnquiryBtn.performClick();
                    return true;
                }
                return false;
            }
        });
        userType = root.findViewById(R.id.userType);
        ArrayAdapter<String> stringArrayAdapter = new ArrayAdapter<>(getActivity(), R.layout.user_spinner_item,getResources().getStringArray(R.array.user_type));
        userType.setAdapter(stringArrayAdapter);
        userType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedUserType = (String) userType.getSelectedItem();
                //showToastAtCentre(selectedUserType,LENGTH_SHORT);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        submitEnquiryBtn = root.findViewById(R.id.submitEnquiryBtn);
        submitEnquiryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = enquiryNameTxt.getText().toString().trim();
                String mobile = enquiryMobileTxt.getText().toString().trim();
                String email = enquiryMailTxt.getText().toString().trim();
                String enquiry = enquiryTextTxt.getText().toString().trim();

                if(TextUtils.isEmpty(name))
                {
                    showToastAtCentre("Please enter your Name",LENGTH_LONG);
                    return;
                }
                if(TextUtils.isEmpty(mobile))
                {
                    showToastAtCentre("Please enter the Mobile number",LENGTH_LONG);
                    return;
                }
                if(TextUtils.isEmpty(email))
                {
                    showToastAtCentre("Please enter the Email",LENGTH_LONG);
                    return;
                }
                if(TextUtils.isEmpty(enquiry))
                {
                    showToastAtCentre("Please write your query",LENGTH_LONG);
                    return;
                }
                uploadQuery(name,mobile,email,enquiry);
            }
        });

        return root;
    }

    private void uploadQuery(String name, String mobile, String email, String enquiry) {
        String token = SharedPreferenceUtility.getUserToken(getActivity());
        String regId = SharedPreferenceUtility.getUserId(getActivity());

        Utils.showBusyAnimation(baseActivity,"Authenticating..");
        Call<GeneralModel> call = apiService.submitEnquiry(token,"enquiry",regId,name,selectedUserType,mobile,email,enquiry);

        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {

                Utils.hideBusyAnimation(baseActivity);
                if(response.body() != null) {
                    if (response.body().getResponse().get(0).getStatus().equalsIgnoreCase("success"))
                    {
                        showToastAtCentre("Your message has been submitted successfully!",LENGTH_LONG);
                        ((DashboardActivity)getActivity()).replaceFragments("home","");
                    }
                    else // Handle failure cases here
                    {
                        String mess = response.body().getResponse().get(0).getMessage();
                        Log.d(TAG, "Response: " + mess);
                        showToastAtCentre("" + mess, LENGTH_LONG);
                    }
                }

            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                Utils.hideBusyAnimation(baseActivity);
                Log.d(TAG, "Response: " + t.getMessage());
                if (t instanceof IOException) {
                    Toast.makeText(baseActivity, "No Internet Connection!!", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void showToastAtCentre(String message, int duration) {
        if (mToast != null) {
            mToast.cancel();
            mToast = null;
        }
        mToast = Toast.makeText(getActivity().getApplicationContext(), message, duration);
        mToast.show();
    }
}
